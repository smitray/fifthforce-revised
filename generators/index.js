const graph = require('./graphql');
const component = require('./component');
const style = require('./styling');

module.exports = (plop) => {
  plop.setGenerator('graphql', graph);
  plop.setGenerator('component', component);
  plop.setGenerator('style', style);
};
