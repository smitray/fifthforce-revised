/* eslint-disable quotes */
module.exports = {
  description: 'Add a shared styling to the app',
  prompts: [{
    type: 'input',
    name: 'name',
    message: 'What is the name of the shared style?',
    default: 'Button'
  }],
  actions: () => {
    const path = '../components/Styling/{{properCase name}}';

    return [{
      type: 'add',
      path: `${path}/index.js`,
      templateFile: './styling/index.js.hbs',
      abortOnFail: true
    }, {
      type: 'append',
      path: `../components/Styling/index.js`,
      pattern: `/* INJECT_EXPORT */`,
      template: `export { default as {{properCase name}} } from './{{properCase name}}';`
    },
    {
      type: 'append',
      path: '../components/index.js',
      pattern: `/* INJECT_EXPORT */`,
      template: `export { default as {{properCase name}} } from './Styling/{{properCase name}}';`
    }];
  }
};
