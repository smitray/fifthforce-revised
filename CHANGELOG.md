# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [2.0.0](https://gitlab.com/smitray/glosh/compare/v1.1.0...v2.0.0) (2020-04-21)


### ⚠ BREAKING CHANGES

* **[revamp]: new ui and style implementation with revised code:** modules updated

### Features

* **[revamp]: new ui and style implementation with revised code:** desig ([469df21](https://gitlab.com/smitray/glosh/commit/469df2131ea879c05944e8b8915a6e5ec2601293))


### Bug Fixes

* **gql link:** gql link ([4d99c41](https://gitlab.com/smitray/glosh/commit/4d99c413ccd77fb6bbefdebb34a584bf7d47a4fd))
* **random\:** random ([dd32cc7](https://gitlab.com/smitray/glosh/commit/dd32cc782a936288dbf8552cb3f80ffc600ed51e))
* **tables upgraded:** tables ([07bfd35](https://gitlab.com/smitray/glosh/commit/07bfd353a473069cc12133d8970e3ee8611a484f))

## 1.1.0 (2020-04-04)


### Features

* **all signup ready:** all signup ready ([0a8e11b](https://gitlab.com/smitray/glosh/commit/0a8e11b2a1bd8f715b7adc02169cff4b24c66cd4))
* **automation added:** automation added ([8092d7c](https://gitlab.com/smitray/glosh/commit/8092d7ca00be06f35cfbdacd6c0c86441d5e09aa))
* **config updated:** config updated ([ae56b72](https://gitlab.com/smitray/glosh/commit/ae56b727200e75aea4ab51c9af376714c493ccda))
* **course created:** course created ([2a4d948](https://gitlab.com/smitray/glosh/commit/2a4d948dc2ad652dc4577be8eaa530e89fc53f2e))
* **dashboard widget design:** dashboard widget design ([bddcf31](https://gitlab.com/smitray/glosh/commit/bddcf31764978c2cb13e52a009a11d581cdb4078))
* **exam and result ready:** exam and result ready ([97bf038](https://gitlab.com/smitray/glosh/commit/97bf038c88a376fc124c16bb89994e89b6536ab6))
* **initial step:** initial step ([2dd2f66](https://gitlab.com/smitray/glosh/commit/2dd2f66b8714400abfb02204e3c7382fb2c92579))
* **paper complete:** paper complete ([0a34446](https://gitlab.com/smitray/glosh/commit/0a34446d63bce5efd8c9b66f871db0c21bca86f1))
* **question bulk added:** question bulk added ([aa7a194](https://gitlab.com/smitray/glosh/commit/aa7a194c661cebff94a626ee1a3ac6c330b56120))
* **question list view:** question list view ([9fa255b](https://gitlab.com/smitray/glosh/commit/9fa255ba0f5b6bee1d020f068f85d11a570354ea))
* **question list view completed:** question list view completed ([8f7dd77](https://gitlab.com/smitray/glosh/commit/8f7dd771a3244a0f6c6ef574c4f25dcc8da0215f))
* **question part done:** question part done ([345c032](https://gitlab.com/smitray/glosh/commit/345c0326b1baae9f8103b6317b7e8a4e770bde4e))
* **question window created:** question window created ([fde2f41](https://gitlab.com/smitray/glosh/commit/fde2f41b40b0e68cb2196e18119e0c7e14920147))
* **student signup complete:** student signup complete ([1660172](https://gitlab.com/smitray/glosh/commit/16601726d0e1ca5b157f68b93eb28c4d22cd7563))
* **student signup ready:** student signup ready ([0213185](https://gitlab.com/smitray/glosh/commit/02131852c5d3b1db42226e741ad44622fb695f99))
* **syllabus added:** syllabus added ([4d44b91](https://gitlab.com/smitray/glosh/commit/4d44b9122a6fd2987d3e88ddcaa514c5c57f061d))
* **system data:** system data ([6e9a093](https://gitlab.com/smitray/glosh/commit/6e9a0935a5d6ada0eabd5dfd51afbfe5032a85de))
* **test generate:** test generate ([84261ea](https://gitlab.com/smitray/glosh/commit/84261eaf07ec0de704f1b58295d30ae0602bdf7c))
* **test generate:** test generate ([b2d4e04](https://gitlab.com/smitray/glosh/commit/b2d4e04aa73a8a5d4a31240c853f298e9f165d75))
* **test generate complete:** test generate ([afc16ed](https://gitlab.com/smitray/glosh/commit/afc16ed88cb78729e2247e70a4788a03feb8b643))
* **update:** update ([aeec47d](https://gitlab.com/smitray/glosh/commit/aeec47d8c880b8c4355276d3c1a8fb7372b4392b))


### Bug Fixes

* **fix in syllabus:** fix in syllabus ([faca133](https://gitlab.com/smitray/glosh/commit/faca1332997716f30c981a5068bcf6b4b5f18253))
* **getinitprop:** getInitProp ([a6c06aa](https://gitlab.com/smitray/glosh/commit/a6c06aacf7a357d0313d8a141dafd8d171323c31))
* **gnerator path fix:** generator path fix ([86d045f](https://gitlab.com/smitray/glosh/commit/86d045f8cbfb369910401e068c7464275ab3d0c3))
* **naming:** nameing ([4e0d9b0](https://gitlab.com/smitray/glosh/commit/4e0d9b09a99738505acd4e82e45063638c7a61ea))
* **paper generate fixed:** paper generate fixed ([b9d1c61](https://gitlab.com/smitray/glosh/commit/b9d1c61af2665de6cbca459a03e9c7a90a0b6e82))
* **question resolver:** question resolver ([375d9f4](https://gitlab.com/smitray/glosh/commit/375d9f4f6959d9a39f8e45c9614cc781b0f0044e))
* **question view window:** question view window ([4ae14ca](https://gitlab.com/smitray/glosh/commit/4ae14ca9fe4a4c689bd41fc8df04338583002c8b))
* **removed gitmoji:** removed gitmoji ([73d8348](https://gitlab.com/smitray/glosh/commit/73d8348df60362117ac2fc497e021884fe8ebd56))
* **signout:** signout ([3efb11d](https://gitlab.com/smitray/glosh/commit/3efb11dcd03caec0dd5573da35b778e1f39cd504))
* **some update:** some update ([73fc20b](https://gitlab.com/smitray/glosh/commit/73fc20b8de2cea56e80ee9dc6f4292c163ef614d))
* **static:** static ([ff2cc7d](https://gitlab.com/smitray/glosh/commit/ff2cc7d53f02ef7ac396a5d6f9c6df77f6f93699))
* **static deleted:** static deleted ([87da864](https://gitlab.com/smitray/glosh/commit/87da864228435b3dca3033c1104d15f2755114f9))
* **static deleted:** static deleted ([1be1efd](https://gitlab.com/smitray/glosh/commit/1be1efde276895da418a9c589562778400b95838))
* **static files deleted:** static files deleted ([33fc451](https://gitlab.com/smitray/glosh/commit/33fc4517a1e9f37c69246dd78639610faaa1ff01))
* **sy fix:** sy fix ([15ef749](https://gitlab.com/smitray/glosh/commit/15ef74961bea3f94c5e95e0076f98f475485a241))
* **test:** test ([3a6fd0f](https://gitlab.com/smitray/glosh/commit/3a6fd0f656b9597bd1bc45c4df1a233be27bb0c2))
* **testing automation:** testing automation ([b8b20ee](https://gitlab.com/smitray/glosh/commit/b8b20ee55ba7ca4a0aaa8da3b964d17d1930b0c7))
* **update:** update ([62b63ec](https://gitlab.com/smitray/glosh/commit/62b63ec34b71a3c7f346ac42171761077a17c40b))
