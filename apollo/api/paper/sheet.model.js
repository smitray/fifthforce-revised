import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';

const sheetSchema = new mongoose.Schema({
  paperId: {
    type: mongoose.Types.ObjectId,
    required: true
  },
  questionId: {
    type: mongoose.Types.ObjectId,
    required: true
  }
});

sheetSchema.plugin(uniqueValidator);
sheetSchema.plugin(timestamp);

export default mongoose.models.sheetModel || mongoose.model('sheetModel', sheetSchema);
