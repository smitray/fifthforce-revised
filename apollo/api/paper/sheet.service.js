import Crud from '~lib/Crud';
import sheetModel from './sheet.model';

class SheetService extends Crud {}

const sheetCrud = new SheetService(sheetModel);

export default sheetCrud;
