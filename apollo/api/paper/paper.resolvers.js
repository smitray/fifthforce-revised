import {
  UserInputError,
  AuthenticationError
} from 'apollo-server-micro';

import PaperService from './paper.service';

let paper;


const paperResolvers = {
  // Query: {

  // },
  // Paper: {

  // },
  Mutation: {
    paperCreate: async (root, {
      courseId,
      subjectId,
      topics,
      time,
      question,
      level,
      activation
    }, {
        userId,
        accType,
        accountId
      }) => {
      if (!accountId) {
        throw new AuthenticationError('User must be authenticated');
      }
      paper = await PaperService.generatePaper({
        courseId,
        subjectId,
        topics,
        time,
        question,
        level,
        activation,
        userId,
        accType
      });

      return paper;
    }
  }
};

export default paperResolvers;
