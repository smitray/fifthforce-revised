import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';

const paperChapterSchema = new mongoose.Schema({
  paperId: {
    type: mongoose.Types.ObjectId,
    required: true
  },
  topicId: {
    type: mongoose.Types.ObjectId,
    required: true
  }
});

paperChapterSchema.plugin(uniqueValidator);
paperChapterSchema.plugin(timestamp);

export default mongoose.models.paperChapterModel || mongoose.model('paperChapterModel', paperChapterSchema);
