/* eslint-disable class-methods-use-this */
import { UserInputError } from 'apollo-server-micro';
import { filter, includes, has } from 'lodash';
import Crud from '~lib/Crud';
import paperModel from './paper.model';
import { questionService } from '../question';
import sheetService from './sheet.service';
import paperChapterService from './chapter.service';

const saveNormal = async (data) => {
  const question = await sheetService.create(data);
  return question;
};

const savePhrase = async ({
  data,
  paperId
}) => {
  let qts = [];
  data.forEach(({ _id }) => {
    qts = [
      ...qts,
      {
        questionId: _id,
        paperId
      }
    ];
  });
  await saveNormal(qts);
};

const questionGenerator = ({
  allQts,
  papers,
  question
}) => {
  let allPhrase = filter(allQts, (e) => includes(['phrase'], e.qType));
  let allNormal = filter(allQts, (e) => includes(['normal'], e.qType));
  const backUpData = [];


  let mixType = true;
  if (!allNormal.length || !allPhrase.length) {
    mixType = false;
  }


  const processQuestion = ({
    isBackup,
    level,
    paperId,
    isNormal
  }) => {
    let qIndex;
    let qCounter;
    let source;
    let questionCount;
    if (isBackup) {
      qCounter = isNormal ? allNormal.length : allPhrase.length;
      qIndex = Math.floor(Math.random() * qCounter);

      if (isNormal) {
        source = allNormal[qIndex];
        if (!has(backUpData[level], 'normal')) {
          const dummy = backUpData[level];
          backUpData[level] = {
            ...dummy,
            normal: []
          };
        }
        backUpData[level].normal.push(source);
        allNormal = allNormal.slice(0, qIndex)
          .concat(allNormal.slice(qIndex + 1, allNormal.length));
        saveNormal({
          questionId: source._id,
          paperId
        });
        questionCount += 1;
      } else {
        source = allPhrase[qIndex];
        if (!has(backUpData[level], 'phrase')) {
          const dummy = backUpData[level];
          backUpData[level] = {
            ...dummy,
            phrase: []
          };
        }
        backUpData[level].phrase.push(source);
        allPhrase = allPhrase.slice(0, qIndex)
          .concat(allPhrase.slice(qIndex + 1, allPhrase.length));

        savePhrase({
          data: source.subQuestion,
          paperId
        });
        questionCount = source.subQuestion.length;
      }
    } else {
      qCounter = isNormal ? backUpData[level].normal.length : backUpData[level].phrase.length;
      qIndex = Math.floor(Math.random() * qCounter);

      if (isNormal) {
        source = backUpData[level].normal[qIndex];
        backUpData[level].normal = backUpData[level].normal.slice(0, qIndex)
          .concat(backUpData[level].normal.slice(qIndex + 1, backUpData[level].normal.length));

        saveNormal({
          questionId: source._id,
          paperId
        });
        questionCount += 1;
      } else {
        source = backUpData[level].phrase[qIndex];
        backUpData[level].phrase = backUpData[level].phrase.slice(0, qIndex)
          .concat(backUpData[level].phrase.slice(qIndex + 1, backUpData[level].phrase.length));

        savePhrase({
          data: source.subQuestion,
          paperId
        });
        questionCount = source.subQuestion.length;
      }
    }

    return questionCount;
  };


  const sets = papers.map((qSet) => {
    const level = qSet.map((lvl) => {
      let count = 0;
      let normal = 0;
      let phrase = 0;
      let repeat = 0;
      let phraseCount;
      while (count < question) {
        if (mixType) {
          if (allPhrase.length && ((question * 0.4) > count)) {
            phraseCount = processQuestion({
              isNormal: false,
              isBackup: true,
              level: lvl.level,
              paperId: lvl._id
            });

            count += phraseCount;
            phrase += phraseCount;
          } else if (allNormal.length) {
            processQuestion({
              isBackup: true,
              level: lvl.level,
              paperId: lvl._id,
              isNormal: true
            });

            count += 1;
            normal += 1;
          } else if (!allNormal.length) {
            if (allPhrase.length && ((question * 0.4) > count)) {
              phraseCount = processQuestion({
                isNormal: false,
                isBackup: true,
                level: lvl.level,
                paperId: lvl._id
              });

              count += phraseCount;
              phrase += phraseCount;
            } else if (!allPhrase.length && ((question * 0.4) > count)) {
              phraseCount = processQuestion({
                isBackup: false,
                level: lvl.level,
                paperId: lvl._id,
                isNormal: false
              });
              count += phraseCount;
              phrase += phraseCount;
              repeat += phraseCount;
            } else {
              processQuestion({
                isBackup: false,
                level: lvl.level,
                paperId: lvl._id,
                isNormal: true
              });

              count += 1;
              normal += 1;
              repeat += 1;
            }
          }
        }


        if (!mixType) {
          if (allNormal.length) {
            processQuestion({
              isBackup: true,
              level: lvl.level,
              paperId: lvl._id,
              isNormal: true
            });

            count += 1;
            normal += 1;
          } else if (!allNormal.length) {
            processQuestion({
              isBackup: false,
              level: lvl.level,
              paperId: lvl._id,
              isNormal: true
            });

            count += 1;
            normal += 1;
            repeat += 1;
          } else if (allPhrase.length) {
            phraseCount = processQuestion({
              count,
              isNormal: false,
              isBackup: true,
              level: lvl.level,
              paperId: lvl._id
            });

            count += phraseCount;
            phrase += phraseCount;
          } else if (!allPhrase.length) {
            phraseCount = processQuestion({
              count,
              isNormal: false,
              isBackup: false,
              level: lvl.level,
              paperId: lvl._id
            });

            count += phraseCount;
            phrase += phraseCount;
            repeat += phraseCount;
          }
        }
      }
      return {
        level: lvl.level,
        set: lvl.set,
        normal,
        phrase,
        repeat
      };
    });
    return level;
  });

  return sets;
};

class PaperService extends Crud {
  async generatePaper({
    courseId,
    subjectId,
    topics,
    time,
    question,
    level,
    activation,
    userId,
    accType
  }) {
    let levelCount = [1, 2, 3];
    const setCount = [1, 2, 3];
    let totalQts = 0;
    const questions = await Promise.all(topics.map(async (chapter) => {
      const qts = await questionService.get({
        qr: {
          chapterId: chapter.topicId,
          subjectId,
          parent: true
        }
      });
      let allQts = [];
      await Promise.all(qts.map(async (item) => {
        if (item.qType !== 'phrase') {
          allQts = [...allQts, {
            _id: item._id,
            qType: item.qType
          }];
          totalQts += 1;
        } else {
          const subQts = await questionService.get({
            qr: {
              questionId: item._id
            },
            select: '_id'
          });
          allQts = [
            ...allQts,
            {
              _id: item._id,
              qType: item.qType,
              subQuestion: subQts
            }
          ];
          totalQts += subQts.length;
        }
      }));
      return allQts;
    }))
      .then(async (qts) => qts)
      .catch((error) => {
        throw new UserInputError('Questions collections failed', {
          message: error.message,
          stack: error.stack
        });
      });
    let allQts = [];
    questions.forEach((item) => {
      allQts = [...allQts, ...item];
    });


    if (totalQts < 120) {
      return {
        success: false,
        message: 'Not enough question'
      };
    }

    if (level) {
      levelCount = [1];
    }
    const sets = await Promise.all(setCount.map(async (qSets) => {
      const papers = await Promise.all(levelCount.map(async (lvl) => {
        const setX = {
          set: qSets,
          level: level || lvl,
          courseId,
          subjectId,
          time,
          activation: new Date(activation),
          status: 'created'
        };
        if (accType === 'school' || accType === 'tutor') {
          setX.thirdParty = userId;
        }
        const set = await this.create(setX);
        let paperChapter = [];
        topics.forEach((topic) => {
          paperChapter = [
            ...paperChapter,
            {
              paperId: set._id,
              topicId: topic.topicId
            }
          ];
        });
        await paperChapterService.create(paperChapter);
        return set;
      }))
        .then((result) => result);
      return [
        ...papers
      ];
    }))
      .then((result) => result)
      .catch((error) => {
        throw new UserInputError('Sheet create failed', {
          message: error.message,
          stack: error.stack
        });
      });
    const result = questionGenerator({
      question,
      papers: sets,
      allQts
    });
    return {
      success: true,
      message: 'Paper generated successfully',
      paper: result
    };
  }
}

const paperCrud = new PaperService(paperModel);

export default paperCrud;
