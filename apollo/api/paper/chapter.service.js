import Crud from '~lib/Crud';
import paperChapterModel from './chapter.model';

class PaperChapterService extends Crud {}

const paperChapterCrud = new PaperChapterService(paperChapterModel);

export default paperChapterCrud;
