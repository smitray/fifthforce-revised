import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';

const paperSchema = new mongoose.Schema({
  set: {
    type: Number,
    default: 1
  },
  level: {
    type: Number,
    required: true
  },
  courseId: {
    type: mongoose.Types.ObjectId,
    required: true
  },
  subjectId: {
    type: mongoose.Types.ObjectId,
    required: true
  },
  time: {
    type: Number,
    required: true
  },
  activation: {
    type: Date
  },
  status: {
    type: String
  },
  thirdParty: {
    type: mongoose.Types.ObjectId
  }
});

paperSchema.plugin(uniqueValidator);
paperSchema.plugin(timestamp);

export default mongoose.models.paperModel || mongoose.model('paperModel', paperSchema);
