import { gql } from 'apollo-server-micro';

const paperDefs = gql`

  type Paper {
    _id: String,
    set: Int,
    level: Int,
    course: Course,
    subject: Subject,
    time: Int,
    question: [Question],
    topic: [Topic],
    activation: DateTime,
    status: String
  }

  input topicInput {
    topicId: String
  }

  # extend type Query {
  #   papers(
  #     courseId: String!,
  #     subjectId: String!,
  #     level: Int
  #   ): [Paper],
  #   paper(
  #     _id: String!
  #   ): Paper
  #   approvedPapers(
  #     status: String!,
  #     courseId: String,
  #     subjectId: String,
  #     level: Int
  #   ): [Paper]
  # }

  extend type Mutation {
    paperCreate(
      courseId: String!,
      subjectId: String!,
      topics: [topicInput!]!,
      time: Int,
      question: Int,
      level: Int,
      activation: DateTime
    ): [Paper]
    # paperUpdate(
    #   _id: String!
    #   status: String,
    #   password: String
    # ): Paper
    # paperDelete(
    #   _id: String!
    # ): Paper
  }

`;

export default paperDefs;
