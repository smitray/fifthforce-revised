import {
  UserInputError
} from 'apollo-server-micro';

import Crud from '~lib/Crud';
import userModel from './user.model';

import { accountService } from '../account';

class UserService extends Crud {
  async userCreate({
    _id,
    name,
    dob,
    gender,
    accountId
  }) {
    try {
      const user = await this.put({
        params: {
          qr: {
            _id
          }
        },
        body: {
          name,
          dob: new Date(dob),
          gender
        }
      });
      await accountService.put({
        params: {
          qr: {
            _id: accountId
          }
        },
        body: {
          part: 1
        }
      });
      return user;
    } catch (error) {
      throw new UserInputError('Wrong input provided', {
        message: error.message,
        stack: error.stack
      });
    }
  }
}

const userCrud = new UserService(userModel);

export default userCrud;
