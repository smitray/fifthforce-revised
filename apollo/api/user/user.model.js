import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';

const userSchema = new mongoose.Schema({
  name: {
    type: String
  },
  gender: {
    type: String
  },
  avatar: {
    type: mongoose.Types.ObjectId
  },
  dob: {
    type: Date
  },
  accountId: {
    type: mongoose.Types.ObjectId,
    required: true
  },
  schoolId: {
    type: mongoose.Types.ObjectId
  },
  documentId: {
    type: mongoose.Types.ObjectId
  }
});

userSchema.plugin(uniqueValidator);
userSchema.plugin(timestamp);

export default mongoose.models.userModel || mongoose.model('userModel', userSchema);
