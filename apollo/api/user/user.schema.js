import { gql } from 'apollo-server-micro';

const userDefs = gql`

  type User {
    _id: String,
    name: String,
    dob: DateTime,
    account: Account,
    gender: String
    # avatar: File,
    # student: Student,
    # school: School,
    # document: File
  }

  extend type Query {
    myself: User
  }

  extend type Mutation {
    userCreate(
      name: String!,
      dob: DateTime!,
      gender: String!
    ): User
  }

`;

export default userDefs;
