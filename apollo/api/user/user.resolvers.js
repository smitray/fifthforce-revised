import {
  AuthenticationError,
  UserInputError
} from 'apollo-server-micro';
import userService from './user.service';
import { accountService } from '../account';
// import { fileService } from '../file';
// import { studentService } from '../student';
// import { schoolService } from '../school';

let user;

const userResolvers = {
  Query: {
    myself: async (root, _, {
      userId
    }) => {
      try {
        const myself = await userService.single({
          qr: {
            _id: userId
          }
        });
        return myself;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    }
  },
  User: {
    account: async ({ accountId }) => {
      try {
        const account = await accountService.single({
          qr: {
            _id: accountId
          }
        });
        return account;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    }
    // avatar: async ({ avatar }) => {
    //   try {
    //     const avt = await fileService.single({
    //       qr: {
    //         _id: avatar
    //       }
    //     });
    //     return avt;
    //   } catch (error) {
    //     throw new UserInputError('Wrong input provided', {
    //       message: error.message,
    //       stack: error.stack
    //     });
    //   }
    // },
    // student: async ({ _id }) => {
    //   try {
    //     const stData = await studentService.single({
    //       qr: {
    //         userId: _id
    //       }
    //     });
    //     return stData;
    //   } catch (error) {
    //     throw new UserInputError('Wrong input provided', {
    //       message: error.message,
    //       stack: error.stack
    //     });
    //   }
    // },
    // school: async ({ school }) => {
    //   try {
    //     const schl = await schoolService.single({
    //       qr: {
    //         _id: school
    //       }
    //     });
    //     return schl;
    //   } catch (error) {
    //     throw new UserInputError('Wrong input provided', {
    //       message: error.message,
    //       stack: error.stack
    //     });
    //   }
    // }
  },
  Mutation: {
    userCreate: async (root, content, {
      userId,
      accountId
    }) => {
      if (!userId) {
        throw new AuthenticationError('User must be authenticated');
      }
      user = userService.userCreate({
        ...content,
        _id: userId,
        accountId
      });
      return user;
    }
  }
};

export default userResolvers;
