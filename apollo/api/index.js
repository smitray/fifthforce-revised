import { merge } from 'lodash';
import {
  DateTimeScalar,
  DateTime
} from '@saeris/graphql-scalars';

/* INJECT_IMPORT */
import { paperDefs, paperResolvers } from './paper';
import { answerDefs, answerResolvers } from './answer';
import { questionDefs, questionResolvers } from './question';
import { fileDefs, fileResolvers } from './file';
import { schoolDefs, schoolResolvers } from './school';
import { topicDefs, topicResolvers } from './topic';
import { subjectDefs, subjectResolvers } from './subject';
import { districtDefs, districtResolvers } from './district';
import { courseDefs, courseResolvers } from './course';
import { userDefs, userResolvers } from './user';
import { accountDefs, accountResolvers } from './account';


const typeDefs = [
  /* INJECT_DEFS */
  paperDefs,
  answerDefs,
  questionDefs,
  fileDefs,
  schoolDefs,
  topicDefs,
  subjectDefs,
  districtDefs,
  courseDefs,
  userDefs,
  accountDefs
];

const resol = merge(
  /* INJECT_RESOLVERS */
  paperResolvers,
  answerResolvers,
  questionResolvers,
  fileResolvers,
  schoolResolvers,
  topicResolvers,
  subjectResolvers,
  districtResolvers,
  courseResolvers,
  userResolvers,
  accountResolvers
);

export const resolvers = {
  ...resol,
  DateTime
};


export const graphDefs = [
  ...typeDefs,
  DateTimeScalar
];
