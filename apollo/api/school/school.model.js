import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';

const schoolSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  address: {
    type: String
  },
  published: {
    type: Boolean,
    default: true
  },
  registered: {
    type: Boolean,
    default: false
  },
  phone: {
    type: String
  },
  medium: {
    type: String
  },
  sType: {
    type: String
  },
  districtId: {
    type: mongoose.Types.ObjectId,
    required: true
  }
});

schoolSchema.plugin(uniqueValidator);
schoolSchema.plugin(timestamp);

export default mongoose.models.schoolModel || mongoose.model('schoolModel', schoolSchema);
