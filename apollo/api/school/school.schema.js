import { gql } from 'apollo-server-micro';

const schoolDefs = gql`

  type School {
    _id: String,
    name: String,
    location: District,
    address: String,
    phone: String,
    medium: String,
    sType: String
    published: Boolean,
    registered: Boolean
  }

  extend type Query {
    schools(
      districtId: String!
    ): [School],
    school(
      _id: String!
    ): School
  }

  extend type Mutation {
    schoolCreate(
      districtId: String!
      name: String
      address: String
      phone: String
      medium: String
      sType: String
    ): School
    schoolUpdate(
      _id: String!
      name: String
      address: String
      phone: String
      medium: String
      sType: String
    ): School
    schoolDelete(
      _id: String!
    ): School
    schoolUpload(
      filepath: String!
    ): [School]
  }

`;

export default schoolDefs;
