import { UserInputError, AuthenticationError } from 'apollo-server-micro';
import schoolService from './school.service';

import { districtService } from '../district';

let school;

const schoolResolvers = {
  Query: {
    schools: async (root, {
      districtId
    }) => {
      try {
        const schools = await schoolService.get({
          qr: {
            districtId,
            published: true
          }
        });
        return schools;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    },
    school: async (root, { _id }) => {
      try {
        school = await schoolService.single({
          qr: {
            _id
          }
        });
        return school;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    }
  },
  School: {
    location: async ({ districtId }) => {
      try {
        const location = await districtService.single({
          qr: {
            _id: districtId
          }
        });
        return location;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    }
  },
  Mutation: {
    schoolCreate: async (root, {
      districtId,
      name,
      address,
      phone,
      medium,
      sType
    }, { accountId }) => {
      if (!accountId) {
        throw new AuthenticationError('User must be authenticated');
      }
      try {
        school = await schoolService.create({
          districtId,
          name,
          address,
          phone,
          medium,
          sType
        });
        return school;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    },
    schoolUpdate: async (root, {
      _id,
      name,
      address,
      phone,
      medium,
      sType
    }, { accountId }) => {
      if (!accountId) {
        throw new AuthenticationError('User must be authenticated');
      }
      try {
        school = await schoolService.put({
          params: {
            qr: {
              _id
            }
          },
          body: {
            name,
            address,
            phone,
            medium,
            sType
          }
        });
        return school;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    },
    schoolDelete: async (root, { _id }, { accountId }) => {
      if (!accountId) {
        throw new AuthenticationError('User must be authenticated');
      }
      try {
        school = await schoolService.delete({
          params: {
            qr: {
              _id
            }
          }
        });
        return school;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    }
  }
};

export default schoolResolvers;
