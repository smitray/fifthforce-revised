// import { UserInputError } from 'apollo-server-micro';

import Crud from '~lib/Crud';
import schoolModel from './school.model';


class SchoolService extends Crud {}

const schoolCrud = new SchoolService(schoolModel);

export default schoolCrud;
