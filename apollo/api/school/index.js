export { default as schoolModel } from './school.model';
export { default as schoolService } from './school.service';
export { default as schoolDefs } from './school.schema';
export { default as schoolResolvers } from './school.resolvers';
