import { gql } from 'apollo-server-micro';

const answerDefs = gql`

  type Answer {
    _id: String,
    answer: String,
    correct: String
  }

  extend type Query {
    answer(
      _id: String!
    ): Answer
  }

  extend type Mutation {
    answerCreate(
      answer: String!,
      correct: String,
      questionId: String!
    ): Answer
    answerUpdate(
      _id: String!
      answer: String,
      correct: String
    ): Answer
    answerDelete(
      _id: String!
    ): Answer
  }

`;

export default answerDefs;
