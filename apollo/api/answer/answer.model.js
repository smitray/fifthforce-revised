import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';

const answerSchema = new mongoose.Schema({
  answer: {
    type: String
  },
  correct: {
    type: String
  },
  questionId: {
    type: mongoose.Types.ObjectId
  }
});

answerSchema.plugin(uniqueValidator);
answerSchema.plugin(timestamp);

export default mongoose.models.answerModel || mongoose.model('answerModel', answerSchema);
