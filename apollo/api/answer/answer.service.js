import Crud from '~lib/Crud';
import answerModel from './answer.model';

class AnswerService extends Crud {
  // constructor(model) {
  //   super(model);
  // }
}

const answerCrud = new AnswerService(answerModel);

export default answerCrud;
