import {
  AuthenticationError,
  UserInputError
} from 'apollo-server-micro';
import answerService from './answer.service';

let answers;

const answerResolvers = {
  Query: {
    answer: async (root, { _id }) => {
      try {
        const answer = await answerService.single({
          qr: {
            _id
          }
        });
        return answer;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    }
  },
  Mutation: {
    answerCreate: async (root, {
      answer,
      correct,
      questionId
    }, {
        accountId
      }) => {
      if (!accountId) {
        throw new AuthenticationError('User must be authenticated');
      }
      try {
        answers = await answerService.create({
          answer,
          correct,
          questionId
        });
        return answers;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    },
    answerUpdate: async (root, {
      _id,
      answer,
      correct
    }, {
        accountId
      }) => {
      if (!accountId) {
        throw new AuthenticationError('User must be authenticated');
      }
      try {
        answers = await answerService.put({
          params: {
            qr: {
              _id
            }
          },
          body: {
            answer,
            correct
          }
        });
        return answers;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    },
    answerDelete: async (root, { _id }, {
      accountId
    }) => {
      if (!accountId) {
        throw new AuthenticationError('User must be authenticated');
      }
      try {
        answers = await answerService.delete({
          params: {
            qr: {
              _id
            }
          }
        });
        return answers;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    }
  }
};

export default answerResolvers;
