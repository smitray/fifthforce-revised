import {
  AuthenticationError,
  UserInputError
} from 'apollo-server-micro';
import subjectService from './subject.service';
import { courseService } from '../course';
import { topicService } from '../topic';

let subject;

const subjectResolvers = {
  Query: {
    subjects: async (root, { classId }) => {
      try {
        const subjects = await subjectService.get({
          qr: {
            classId
          }
        });
        return subjects;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    },
    subject: async (root, { _id }) => {
      try {
        subject = await subjectService.single({
          qr: {
            _id
          }
        });
        return subject;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    }
  },
  Subject: {
    course: async ({ classId }) => {
      try {
        const standard = await courseService.single({
          qr: {
            _id: classId
          }
        });
        return standard;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    },
    topic: async ({ _id }) => {
      try {
        const chapter = await topicService.get({
          qr: {
            subjectId: _id
          }
        });
        return chapter;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    }
  },
  Mutation: {
    subjectCreate: async (root, {
      name,
      classId,
      group,
      compulsory
    }, { accountId }) => {
      if (!accountId) {
        throw new AuthenticationError('User must be authenticated');
      }
      try {
        subject = await subjectService.create({
          name,
          classId,
          group,
          compulsory
        });
        return subject;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    },
    subjectUpdate: async (root, {
      _id,
      name,
      group,
      compulsory
    }, { accountId }) => {
      if (!accountId) {
        throw new AuthenticationError('User must be authenticated');
      }
      try {
        subject = await subjectService.put({
          params: {
            qr: {
              _id
            }
          },
          body: {
            name,
            group,
            compulsory
          }
        });
        return subject;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    },
    subjectDelete: async (root, { _id }, { accountId }) => {
      if (!accountId) {
        throw new AuthenticationError('User must be authenticated');
      }
      try {
        subject = await subjectService.delete({
          params: {
            qr: {
              _id
            }
          }
        });
        return subject;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    }
  }
};

export default subjectResolvers;
