import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';
import findOrCreate from 'mongoose-find-or-create';

const subjectSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  group: {
    type: String
  },
  compulsory: {
    type: Boolean
  },
  classId: {
    type: mongoose.Types.ObjectId,
    required: true
  }
});

subjectSchema.plugin(uniqueValidator);
subjectSchema.plugin(timestamp);
subjectSchema.plugin(findOrCreate);

export default mongoose.models.subjectModel || mongoose.model('subjectModel', subjectSchema);
