import { gql } from 'apollo-server-micro';

const subjectDefs = gql`

  type Subject {
    _id: String,
    name: String,
    group: String,
    compulsory: Boolean,
    course: Course,
    topic: [Topic]
  }

  extend type Query {
    subjects(
      classId: String
    ): [Subject]
    subject(
      _id: String!
    ): Subject
  }

  extend type Mutation {
    subjectCreate(
      name: String!
      classId: String!
      group: String
      compulsory: Boolean
    ): Subject
    subjectUpdate(
      _id: String!
      name: String
      group: String
      compulsory: String
    ): Subject
    subjectDelete(
      _id: String!
    ): Subject
  }

`;

export default subjectDefs;
