import Crud from '~lib/Crud';
import subjectModel from './subject.model';

class SubjectService extends Crud {
  // constructor(model) {
  //   super(model);
  // }
}

const subjectCrud = new SubjectService(subjectModel);

export default subjectCrud;
