import { gql } from 'apollo-server-micro';

const courseDefs = gql`

  type Course {
    _id: String,
    name: String,
    subjects: [Subject]
  }

  extend type Query {
    courses: [Course],
    course(
      _id: String!
    ): Course
  }

  extend type Mutation {
    courseCreate(
      name: String
    ): Course
    courseUpdate(
      _id: String!,
      name: String
    ): Course
    courseDelete(
      _id: String!
    ): Course
    courseUpload(
      filepath: String!
    ): [Course]
  }

`;

export default courseDefs;
