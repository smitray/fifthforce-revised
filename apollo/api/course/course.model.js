import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';

const courseSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  }
});

courseSchema.plugin(uniqueValidator);
courseSchema.plugin(timestamp);

export default mongoose.models.courseModel || mongoose.model('courseModel', courseSchema);
