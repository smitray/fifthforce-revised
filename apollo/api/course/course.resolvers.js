import {
  UserInputError,
  AuthenticationError
} from 'apollo-server-micro';
import courseService from './course.service';
import { subjectService } from '../subject';

import xlsx from '~lib/xlsx';

let course;

const courseResolvers = {
  Query: {
    courses: async () => {
      try {
        const courses = await courseService.get({
          sort: {
            name: 'asc'
          }
        });
        return courses;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    },
    course: async (root, { _id }) => {
      try {
        course = await courseService.single({
          qr: {
            _id
          }
        });
        return course;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    }
  },
  Course: {
    subjects: async ({ _id }) => {
      try {
        const subject = await subjectService.get({
          qr: {
            classId: _id
          }
        });
        return subject;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    }
  },
  Mutation: {
    courseUpload: async (root, {
      filepath
    }, { accountId }) => {
      if (!accountId) {
        throw new AuthenticationError('User must be authenticated');
      }
      const data = xlsx({
        filepath,
        sheetName: 'Details'
      });
      course = courseService.bulkUpload(data);
      return course;
    },
    courseCreate: async (root, { name }, {
      accountId
    }) => {
      if (!accountId) {
        throw new AuthenticationError('User must be authenticated');
      }
      try {
        course = await courseService.create({
          name
        });
        return course;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    },
    courseUpdate: async (root, { _id, name }, {
      accountId
    }) => {
      if (!accountId) {
        throw new AuthenticationError('User must be authenticated');
      }
      try {
        course = await courseService.put({
          params: {
            qr: {
              _id
            }
          },
          body: {
            name
          }
        });
        return course;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    },
    courseDelete: async (root, { _id }, {
      accountId
    }) => {
      if (!accountId) {
        throw new AuthenticationError('User must be authenticated');
      }
      try {
        course = await courseService.delete({
          params: {
            qr: {
              _id
            }
          }
        });
        return course;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    }
  }
};

export default courseResolvers;
