import { UserInputError } from 'apollo-server-micro';
import { uniqBy } from 'lodash';
import Crud from '~lib/Crud';
import courseModel from './course.model';
import { subjectService } from '../subject';

class CourseService extends Crud {
  async bulkUpload(data) {
    const uniqClasses = uniqBy(data, 'Course');
    let masterCourse = [];
    await Promise.all(uniqClasses.map(async (course) => {
      let newCourse = await this.single({
        qr: {
          name: course.Course
        }
      });
      if (!newCourse) {
        newCourse = await this.create({
          name: course.Course
        });
      }
      masterCourse = [
        ...masterCourse,
        newCourse
      ];

      const uniqueSubjectsInClass = uniqBy(data.filter((e) => e.Course === course.Course), 'Subject');

      await Promise.all(uniqueSubjectsInClass.map(async (subject) => {
        let newSubject = await subjectService.single({
          qr: {
            name: subject.Subject,
            classId: newCourse._id
          }
        });
        if (!newSubject) {
          newSubject = await subjectService.create({
            name: subject.Subject,
            classId: newCourse._id,
            group: subject.Group || 'nor',
            compulsory: subject.Compulsory
          });
        }
      }));
    }))
      .catch((error) => {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      });
    return masterCourse;
  }
}

const courseCrud = new CourseService(courseModel);

export default courseCrud;
