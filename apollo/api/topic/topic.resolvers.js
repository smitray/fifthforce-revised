import { UserInputError, AuthenticationError } from 'apollo-server-micro';
import topicService from './topic.service';

let topic;

const topicResolvers = {
  Query: {
    topics: async (root, { subjectId }) => {
      try {
        const topics = await topicService.get({
          qr: {
            subjectId
          }
        });
        return topics;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    },
    topic: async (root, { _id }) => {
      try {
        topic = await topicService.single({
          qr: {
            _id
          }
        });
        return topic;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    }
  },
  Mutation: {
    topicCreate: async (root, {
      name,
      subjectId
    }, { accountId }) => {
      if (!accountId) {
        throw new AuthenticationError('User must be authenticated');
      }
      try {
        topic = await topicService.create({
          name,
          subjectId
        });
        return topic;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    },
    topicUpdate: async (root, {
      name,
      _id
    }, { accountId }) => {
      if (!accountId) {
        throw new AuthenticationError('User must be authenticated');
      }
      try {
        topic = await topicService.put({
          params: {
            qr: {
              _id
            }
          },
          body: {
            name
          }
        });
        return topic;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    },
    topicDelete: async (root, { _id }, {
      accountId
    }) => {
      if (!accountId) {
        throw new AuthenticationError('User must be authenticated');
      }
      try {
        topic = await topicService.delete({
          params: {
            qr: {
              _id
            }
          }
        });
        return topic;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    }
  }
};

export default topicResolvers;
