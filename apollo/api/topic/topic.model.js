import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';

const topicSchema = new mongoose.Schema({
  subjectId: {
    type: mongoose.Types.ObjectId,
    required: true
  },
  name: {
    type: String,
    required: true
  }
});

topicSchema.plugin(uniqueValidator);
topicSchema.plugin(timestamp);

export default mongoose.models.topicModel || mongoose.model('topicModel', topicSchema);
