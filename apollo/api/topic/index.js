export { default as topicModel } from './topic.model';
export { default as topicService } from './topic.service';
export { default as topicDefs } from './topic.schema';
export { default as topicResolvers } from './topic.resolvers';
