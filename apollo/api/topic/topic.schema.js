import { gql } from 'apollo-server-micro';

const topicDefs = gql`

  type Topic {
    _id: String,
    name: String,
    subject: Subject
  }

  extend type Query {
    topics(
      subjectId: String!
    ): [Topic],
    topic(
      _id: String!
    ): Topic
  }

  extend type Mutation {
    topicCreate(
      name: String!,
      subjectId: String!
    ): Topic
    topicUpdate(
      _id: String!
      name: String!
    ): Topic
    topicDelete(
      _id: String!
    ): Topic
  }

`;

export default topicDefs;
