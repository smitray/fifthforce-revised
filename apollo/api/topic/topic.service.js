// import { UserInputError, AuthenticationError } from 'apollo-server-micro';
import Crud from '~lib/Crud';
import topicModel from './topic.model';

class TopicService extends Crud {}

const topicCrud = new TopicService(topicModel);

export default topicCrud;
