import { gql } from 'apollo-server-micro';

const accountDefs = gql`

  type Account {
    _id: String,
    username: String,
    email: String,
    phone: String,
    status: String,
    part: Int,
    accType: String,
    token: String,
    user: User
  }

  type AccountVerify {
    message: String!,
    success: Boolean!
  }

  extend type Query {
    emailV(email: String!): AccountVerify,
    phoneV(phone: String!): AccountVerify,
    usernameV(username: String!): AccountVerify
  }

  type Mutation {
    accountCreate(
      username: String!,
      email: String,
      phone: String!,
      password: String!
      accType: String,
    ): Account
    accountUpdate(
      _id: String!
      email: String,
      phone: String,
      password: String
    ): Account
    accountDelete(
      _id: String!
    ): Account
    accountLogin(
      cred: String!,
      password: String!
    ): Account
  }

`;

export default accountDefs;
