import {
  AuthenticationError,
  UserInputError
} from 'apollo-server-micro';
import accountService from './account.service';
import { userService } from '../user';

let account;

const accountResolvers = {
  Query: {
    emailV: async (root, { email }) => {
      try {
        account = await accountService.single({
          qr: {
            email
          }
        });
        if (account) {
          return {
            message: 'Email is already used',
            success: 1
          };
        }
        return {
          message: 'Email is not in use',
          success: 0
        };
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    },
    phoneV: async (root, { phone }) => {
      try {
        account = await accountService.single({
          qr: {
            phone
          }
        });
        if (account) {
          return {
            message: 'Phone is already used',
            success: 1
          };
        }
        return {
          message: 'Phone is not in use',
          success: 0
        };
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    },
    usernameV: async (root, { username }) => {
      try {
        account = await accountService.single({
          qr: {
            username
          }
        });
        if (account) {
          return {
            message: 'Username is already used',
            success: 1
          };
        }
        return {
          message: 'Username is not in use',
          success: 0
        };
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    }
  },
  Account: {
    user: async ({ _id }) => {
      try {
        const user = await userService.single({
          accountId: _id
        });
        return user;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    }
  },
  Mutation: {
    accountCreate: (root, content) => {
      account = accountService.createAccount({
        ...content
      });
      return account;
    },
    accountUpdate: (root, {
      content
    }, {
      accountId
    }) => {
      if (!accountId) {
        throw new AuthenticationError('User must be authenticated');
      }
      account = accountService.updateAccount({
        ...content,
        _id: accountId
      });
      return account;
    },
    accountDelete: async (root, _, { accountId }) => {
      if (!accountId) {
        throw new AuthenticationError('User must be authenticated');
      }
      try {
        account = await accountService.delete({
          params: {
            qr: {
              _id: accountId
            }
          }
        });
        return account;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    },
    accountLogin: (root, credentials) => {
      account = accountService.login({
        ...credentials
      });
      return account;
    }
  }
};

export default accountResolvers;
