import Crud from '~lib/Crud';
import questionImageModel from './image.model';

class QuestionService extends Crud {}

const questionImageCrud = new QuestionService(questionImageModel);

export default questionImageCrud;
