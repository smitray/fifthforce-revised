import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';

const questionImageSchema = new mongoose.Schema({
  questionId: {
    type: mongoose.Types.ObjectId,
    required: true
  },
  fileId: {
    type: mongoose.Types.ObjectId,
    required: true
  }
});

questionImageSchema.plugin(uniqueValidator);
questionImageSchema.plugin(timestamp);

export default mongoose.models.questionImageModel || mongoose.model('questionImageModel', questionImageSchema);
