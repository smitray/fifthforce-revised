import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';

const questionSchema = new mongoose.Schema({
  body: {
    type: String,
    required: true
  },
  qType: {
    type: String,
    default: 'normal'
  },
  score: {
    type: Number
  },
  subjectId: {
    type: mongoose.Types.ObjectId,
    required: true
  },
  chapterId: {
    type: mongoose.Types.ObjectId,
    required: true
  },
  questionId: {
    type: mongoose.Types.ObjectId
  },
  parent: {
    type: Boolean,
    default: true
  },
  thirdParty: {
    type: mongoose.Types.ObjectId
  }
});

questionSchema.plugin(uniqueValidator);
questionSchema.plugin(timestamp);

export default mongoose.models.questionModel || mongoose.model('questionModel', questionSchema);
