import {
  // AuthenticationError,
  UserInputError
} from 'apollo-server-micro';
import { includes } from 'lodash';
import { extname } from 'path';
import { renameSync, existsSync } from 'fs';
import { generate } from 'shortid';

import Crud from '~lib/Crud';
import questionModel from './question.model';
import qtsImgService from './image.service';
import { fileService } from '../file';
import { answerService } from '../answer';

const answerStructure = (qts) => {
  const newCorrect = qts.Correct.split(',');
  return [{
    answer: qts.answerA,
    correct: includes(newCorrect, 'A') ? 'true' : 'false'
  }, {
    answer: qts.answerB,
    correct: includes(newCorrect, 'B') ? 'true' : 'false'
  }, {
    answer: qts.answerC,
    correct: includes(newCorrect, 'C') ? 'true' : 'false'
  }, {
    answer: qts.answerD,
    correct: includes(newCorrect, 'D') ? 'true' : 'false'
  }, {
    answer: qts.answerE,
    correct: includes(newCorrect, 'E') ? 'true' : 'false'
  }, {
    answer: qts.answerF,
    correct: includes(newCorrect, 'F') ? 'true' : 'false'
  }];
};

const createImages = ({ images, questionId }) => {
  const allImages = images.split(',');
  Promise.all(allImages.map(async (img) => {
    const dumpFile = `${process.env.publicPath}/temp/${img.trim()}`;
    if (existsSync(dumpFile)) {
      const ext = extname(images);
      const newFilename = `${Date.now()}-${generate()}${ext}`;
      renameSync(dumpFile, `${process.env.publicPath}/${newFilename}`);
      const file = await fileService.create({
        filename: newFilename,
        filePath: newFilename,
        fileType: 'image'
      });

      await qtsImgService.create({
        questionId,
        fileId: file._id
      });
    }
  }));
};

const createAnswers = async (answers) => {
  await answerService.create(answers);
};

class QuestionService extends Crud {
  async createQuestion(dumpQts) {
    try {
      let answers = null;
      let images = null;
      if (dumpQts.answers) {
        answers = dumpQts.answers;
        delete dumpQts.answers;
      }
      if (dumpQts.imageDetails) {
        images = dumpQts.imageDetails;
        delete dumpQts.imageDetails;
      }
      const newQts = await this.create(dumpQts);
      if (answers) {
        let newAnswer = [];
        answers.forEach((item) => {
          if (item.answer || item.answer.trim().length !== 0) {
            newAnswer = [
              ...newAnswer,
              {
                answer: item.answer.trim(),
                correct: item.correct,
                questionId: newQts._id
              }
            ];
          }
        });
        createAnswers(newAnswer);
      }
      if (images) {
        createImages({
          images,
          questionId: newQts._id
        });
      }
      return newQts;
    } catch (error) {
      throw new UserInputError('Create question error', {
        message: error.message,
        stack: error.stack
      });
    }
  }

  async savePhrase(phrase) {
    const parentQts = phrase.filter((item) => item.parent)[0];
    const subQts = phrase.filter((item) => !item.parent);
    const newParentQts = await this.createQuestion(parentQts);
    subQts.forEach((item) => {
      item.questionId = newParentQts._id;
      this.createQuestion(item);
    });
  }

  processQuestions({
    data,
    subjectId,
    chapterId,
    thirdParty
  }) {
    let count = 0;
    const allPhrase = [];
    const totalQuestions = data.length;
    while (count < totalQuestions) {
      if (data[count].Body) {
        const questionObject = {
          subjectId,
          chapterId,
          body: data[count].Body,
          imageDetails: null || data[count].Image
        };
        if (thirdParty) {
          questionObject.thirdParty = thirdParty;
        }
        if (data[count].Phrase && data[count].Parent) {
          allPhrase.length = 0;
          allPhrase.push({
            ...questionObject,
            qType: 'phrase',
            parent: true
          });
          count += 1;
        } else {
          const answers = answerStructure(data[count]);
          if (data[count].Phrase) {
            allPhrase.push({
              qType: 'phrase',
              parent: false,
              score: data[count].Score,
              answers,
              ...questionObject
            });
            if (data[count].Phrase === 1) {
              this.savePhrase(allPhrase);
            }
          } else {
            this.createQuestion({
              ...questionObject,
              qType: 'normal',
              parent: true,
              score: data[count].Score,
              answers
            });
          }
          count += 1;
        }
      }
    }
    return {
      count
    };
  }
}

const questionCrud = new QuestionService(questionModel);

export default questionCrud;
