import { gql } from 'apollo-server-micro';

const questionDefs = gql`

  type Question {
    _id: String,
    body: String,
    qType: String,
    subject: Subject,
    chapter: Topic,
    file: [File],
    score: Int,
    answers: [Answer],
    subQuestions: [Question],
    parentQts: Question
  }

  type QuestionUpload {
    count: Int
  }

  extend type Query {
    questions(
      subjectId: String!,
      chapterId: String!
    ): [Question],
    question(
      _id: String!
    ): Question
  }

  extend type Mutation {
    questionCreate(
      body: String!,
      qType: String!,
      score: Int,
      imageId: String,
      subjectId: String!,
      chapterId: String!,
      questionId: String,
      parent: Boolean
    ): Question
    questionUpdate(
      _id: String!
      body: String,
      score: Int,
      imageId: String
    ): Question
    questionDelete(
      _id: String!
    ): Question
    questionBulkDelete(
      subjectId: String!
      chapterId: String!
    ): Question
    questionUpload(
      filepath: String!
    ): QuestionUpload
  }

`;

export default questionDefs;
