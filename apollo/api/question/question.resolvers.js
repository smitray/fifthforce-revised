import {
  AuthenticationError,
  UserInputError
} from 'apollo-server-micro';
import qtsImgService from './image.service';
import questionService from './question.service';
import { fileService } from '../file';
import { answerService } from '../answer';
import { subjectService } from '../subject';
import { topicService } from '../topic';
import { courseService } from '../course';

import xlsx from '~lib/xlsx';

let question;

const questionResolvers = {
  Query: {
    questions: async (root, { subjectId, chapterId }, {
      userId,
      accType
    }) => {
      try {
        let questions = [];
        if (subjectId && chapterId) {
          const qr = {
            subjectId,
            chapterId,
            parent: true
          };
          if (accType === 'school' || accType === 'tutor') {
            qr.thirdParty = userId;
          } else {
            qr.thirdParty = {
              $exists: false
            };
          }
          questions = await questionService.get({
            qr
          });
        }
        return questions;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    },
    question: async (root, { _id }) => {
      try {
        question = await questionService.single({
          qr: {
            _id
          }
        });
        return question;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    }
  },
  Question: {
    subject: async ({ subjectId }) => {
      try {
        const subject = await subjectService.single({
          qr: {
            _id: subjectId
          }
        });
        return subject;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    },
    subQuestions: async ({ _id }) => {
      try {
        const questions = await questionService.get({
          qr: {
            questionId: _id
          }
        });
        return questions;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    },
    parentQts: async ({ questionId }) => {
      try {
        const questions = await questionService.single({
          qr: {
            _id: questionId
          }
        });
        return questions;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    },
    chapter: async ({ chapterId }) => {
      try {
        const chapter = await topicService.single({
          qr: {
            _id: chapterId
          }
        });
        return chapter;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    },
    file: async ({ _id }) => {
      const rawData = await qtsImgService.get({
        qr: {
          questionId: _id
        }
      });
      const files = await Promise.all(rawData.map(async (file) => {
        const data = await fileService.single({
          qr: {
            _id: file.fileId
          }
        });
        return data;
      }))
        .then((result) => result)
        .catch((error) => {
          throw new UserInputError('Wrong input provided', {
            message: error.message,
            stack: error.stack
          });
        });
      return [...files];
    },
    answers: async ({ _id }) => {
      try {
        const answers = await answerService.get({
          qr: {
            questionId: _id
          }
        });
        return answers;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    }
  },
  Mutation: {
    questionCreate: async (root, {
      body,
      qType,
      score,
      imageId,
      subjectId,
      chapterId,
      questionId,
      parent
    }, {
        userId,
        accType,
        accountId
      }) => {
      if (!accountId) {
        throw new AuthenticationError('User must be authenticated');
      }
      try {
        const qtsObject = {
          body,
          qType,
          score,
          imageId,
          subjectId,
          chapterId,
          questionId,
          parent
        };
        if (accType === 'school' || accType === 'tutor') {
          qtsObject.thirdParty = userId;
        }
        question = await questionService.create(qtsObject);
        return question;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    },
    questionUpload: async (root, {
      filepath
    }, {
        userId,
        accType,
        accountId
      }) => {
      if (!accountId) {
        throw new AuthenticationError('User must be authenticated');
      }
      try {
        const sheetDetail = xlsx({
          filepath,
          sheetName: 'Details'
        });
        const sheetClass = await courseService.single({
          qr: {
            name: sheetDetail[0].Course
          }
        });
        const sheetSubject = await subjectService.single({
          qr: {
            classId: sheetClass._id,
            name: sheetDetail[0].Subject
          }
        });
        let sheetChapter = await topicService.single({
          qr: {
            subjectId: sheetSubject._id,
            name: sheetDetail[0].Topic
          }
        });
        if (!sheetChapter) {
          sheetChapter = await topicService.create({
            subjectId: sheetSubject._id,
            name: sheetDetail[0].Topic
          });
        }
        const sheetQuestions = xlsx({
          filepath,
          sheetName: 'Questions'
        });
        question = await questionService.processQuestions({
          subjectId: sheetSubject._id,
          chapterId: sheetChapter._id,
          data: sheetQuestions,
          thirdParty: (accType === 'school' || accType === 'tutor') ? userId : null
        });
        return question;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    },
    questionUpdate: async (root, {
      _id,
      body,
      score,
      imageId
    }, {
        accountId
      }) => {
      if (!accountId) {
        throw new AuthenticationError('User must be authenticated');
      }
      try {
        question = await questionService.put({
          params: {
            qr: {
              _id
            }
          },
          body: {
            body,
            score,
            imageId
          }
        });
        return question;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    },
    questionDelete: async (root, { _id }, {
      accountId
    }) => {
      if (!accountId) {
        throw new AuthenticationError('User must be authenticated');
      }
      try {
        question = await questionService.delete({
          params: {
            qr: {
              _id
            }
          }
        });
        return question;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    },
    questionBulkDelete: async (root, {
      subjectId,
      chapterId
    }, {
        accountId
      }) => {
      if (!accountId) {
        throw new AuthenticationError('User must be authenticated');
      }
      try {
        question = await questionService.deleteMany({
          subjectId,
          chapterId
        });
        return question;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    }
  }
};

export default questionResolvers;
