import { UserInputError, AuthenticationError } from 'apollo-server-micro';
import districtService from './district.service';

let district;

const districtResolvers = {
  Query: {
    districts: async () => {
      try {
        const districts = await districtService.get();
        return districts;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    }
  },
  Mutation: {
    districtUpload: async (root, {
      filepath
    }, { accountId }) => {
      if (!accountId) {
        throw new AuthenticationError('User must be authenticated');
      }
      district = districtService.bulkUpload(filepath);
      return district;
    },
    districtCreate: async (root, { name }, {
      accountId
    }) => {
      if (!accountId) {
        throw new AuthenticationError('User must be authenticated');
      }
      try {
        district = await districtService.create({
          name
        });
        return district;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    },
    districtUpdate: async (root, { _id, name }, {
      accountId
    }) => {
      if (!accountId) {
        throw new AuthenticationError('User must be authenticated');
      }
      try {
        district = await districtService.put({
          params: {
            qr: {
              _id
            }
          },
          body: {
            name
          }
        });
        return district;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    },
    districtDelete: async (root, { _id }, {
      accountId
    }) => {
      if (!accountId) {
        throw new AuthenticationError('User must be authenticated');
      }
      try {
        district = await districtService.delete({
          params: {
            qr: {
              _id
            }
          }
        });
        return district;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    }
  }
};

export default districtResolvers;
