import { gql } from 'apollo-server-micro';

const districtDefs = gql`

  type District {
    _id: String,
    name: String
  }

  extend type Query {
    districts: [District]
  }

  extend type Mutation {
    districtCreate(
      name: String
    ): District
    districtUpdate(
      _id: String!,
      name: String
    ): District
    districtDelete(
      _id: String!
    ): District,
    districtUpload(
      filepath: String!
    ): [District]
  }

`;

export default districtDefs;
