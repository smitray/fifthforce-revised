import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';

const districtSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  }
});

districtSchema.plugin(uniqueValidator);
districtSchema.plugin(timestamp);

export default mongoose.models.districtModel || mongoose.model('districtModel', districtSchema);
