import { UserInputError } from 'apollo-server-micro';
import XLSX from 'xlsx';
import Crud from '~lib/Crud';
import districtModel from './district.model';
import cXlsx from '~lib/xlsx';

import { schoolService } from '../school';


class DistrictService extends Crud {
  async bulkUpload(filepath) {
    const workbook = XLSX.readFile(`${process.env.publicPath}/${filepath}`);
    const { SheetNames } = workbook;
    const locations = await Promise.all(SheetNames.map(async (sheet) => {
      const location = await this.create({
        name: sheet
      });
      const data = cXlsx({
        filepath,
        sheetName: sheet
      });
      await Promise.all(data.map(async (schl) => {
        if (schl['SCHOOL NAME/ADDRESS/PHONE']) {
          await schoolService.create({
            name: schl['SCHOOL NAME/ADDRESS/PHONE'],
            address: schl.ADDRESS,
            phone: schl['PHONE NO'],
            medium: schl['MEDIUM/FL'],
            sType: schl['B/G/CO-ED'],
            districtId: location._id
          });
        }
      }));
      return location;
    }))
      .then((result) => result)
      .catch((error) => {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      });
    console.log(locations);
    return locations;
  }
}

const districtCrud = new DistrictService(districtModel);

export default districtCrud;
