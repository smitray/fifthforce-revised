export { default as districtModel } from './district.model';
export { default as districtService } from './district.service';
export { default as districtDefs } from './district.schema';
export { default as districtResolvers } from './district.resolvers';
