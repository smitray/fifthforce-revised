import { gql } from 'apollo-server-micro';

const fileDefs = gql`

  type File {
    _id: String,
    filename: String!,
    filePath: String!,
    fileType: String,
    createdAt: DateTime,
    updatedAt: DateTime
  }

  extend type Query {
    files(
      fileType: String!
    ): [File],
    file(
      _id: String!
    ): File
  }

  extend type Mutation {
    fileCreate(
      file: Upload!
    ): File
    multipleFileCreate(
      files: [Upload!]!,
      temp: Boolean
    ): [File]
    fileDelete(
      _id: String!
    ): File
  }

`;

export default fileDefs;
