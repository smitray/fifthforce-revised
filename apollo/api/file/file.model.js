import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';

const fileSchema = new mongoose.Schema({
  filename: String,
  filePath: String,
  fileType: String
});

fileSchema.plugin(uniqueValidator);
fileSchema.plugin(timestamp);

export default mongoose.models.fileModel || mongoose.model('fileModel', fileSchema);
