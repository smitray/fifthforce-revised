import {
  // AuthenticationError,
  UserInputError
} from 'apollo-server-micro';
import { generate } from 'shortid';
import { join, extname } from 'path';
import { unlinkSync, createWriteStream } from 'fs';
import fileService from './file.service';

const upload = ({
  filename,
  mimetype,
  createReadStream,
  temp
}) => {
  const stream = createReadStream();
  const ext = extname(filename);
  let fileType = mimetype.split('/')[0];
  const newFilename = `${Date.now()}-${generate()}${ext}`;
  let filePath;
  let slugPath;
  if (fileType === 'image') {
    filePath = join(process.env.publicPath, newFilename);
    slugPath = newFilename;
  } else {
    fileType = 'document';
    slugPath = `file/${newFilename}`;
    filePath = join(process.env.publicPath, 'file', newFilename);
  }

  if (temp) {
    filePath = join(process.env.publicPath, 'temp', filename);
  }

  return new Promise((resolve, reject) => {
    stream
      .on('error', (error) => {
        if (stream.truncated) unlinkSync(filePath);
        reject(error);
      })
      .pipe(createWriteStream(filePath))
      .on('finish', async () => {
        if (temp) {
          return resolve({
            filename,
            filePath
          });
        }
        const newFile = await fileService.create({
          filename: newFilename,
          filePath: slugPath,
          fileType
        });
        return resolve(newFile);
      });
  });
};

const fileResolvers = {
  Query: {
    files: async (root, { fileType }) => {
      try {
        const files = await fileService.get({
          qr: {
            fileType
          }
        });
        return files;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    },
    file: async (root, { _id }) => {
      try {
        const file = await fileService.single({
          qr: {
            _id
          }
        });
        return file;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    }
  },
  Mutation: {
    fileCreate: async (root, { file }) => {
      try {
        const { filename, mimetype, createReadStream } = await file;
        const newFile = upload({
          filename,
          mimetype,
          createReadStream
        });
        return newFile;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    },
    multipleFileCreate: async (root, { files, temp }) => Promise.all(files.map(async (file) => {
      const { filename, mimetype, createReadStream } = await file;
      const newFile = upload({
        filename,
        mimetype,
        createReadStream,
        temp
      });
      return newFile;
    }))
      .then((result) => result)
      .catch((error) => {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }),
    fileDelete: async (root, { _id }) => {
      try {
        const file = await fileService.delete({
          params: {
            qr: {
              _id
            }
          }
        });
        const filePath = join(process.env.publicPath, file.filePath);
        unlinkSync(filePath);
        return file;
      } catch (error) {
        throw new UserInputError('Wrong input provided', {
          message: error.message,
          stack: error.stack
        });
      }
    }
  }
};

export default fileResolvers;
