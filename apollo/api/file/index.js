export { default as fileModel } from './file.model';
export { default as fileService } from './file.service';
export { default as fileDefs } from './file.schema';
export { default as fileResolvers } from './file.resolvers';
