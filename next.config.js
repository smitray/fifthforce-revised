const path = require('path');
const cfg = require('./config');

module.exports = {
  cssModules: true,
  env: {
    mongodb: cfg.mongodb,
    GRAPHQL_URL: cfg.GRAPHQL_URL,
    DB_USER: cfg.DB_USER,
    DB_PASSWORD: cfg.DB_PASSWORD,
    publicPath: path.resolve(__dirname, './public'),
    SERVER_SECRET: cfg.SERVER_SECRET
  },
  webpack(config) {
    config.resolve.alias['~components'] = path.resolve(__dirname, './components');
    config.resolve.alias['~app'] = path.resolve(__dirname, './components/App');
    config.resolve.alias['~base'] = path.resolve(__dirname, './components/Base');
    config.resolve.alias['~elements'] = path.resolve(__dirname, './components/WithData');
    config.resolve.alias['~style'] = path.resolve(__dirname, './components/Styling');
    config.resolve.alias['~api'] = path.resolve(__dirname, 'apollo');
    config.resolve.alias['~lib'] = path.resolve(__dirname, 'lib');
    config.resolve.alias['~gql'] = path.resolve(__dirname, 'gql');
    return config;
  }
};
