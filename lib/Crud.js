export default class Crud {
  constructor(model) {
    this.model = model;
  }

  findOrCreate(options) {
    return new Promise((resolve, reject) => {
      this.model.findOrCreate(
        options?.qr || {},
        options?.body || {},
        (err, result) => {
          if (err) {
            reject(err);
          }
          resolve(result);
        }
      );
    });
  }

  get(options) {
    return new Promise((resolve, reject) => {
      this.model.find(options?.qr || {})
        .select(options?.select || {})
        .populate(options?.populate || '')
        .sort(options?.sort || {})
        .exec()
        .then((result) => {
          resolve(result);
        })
        .catch((e) => {
          reject(e);
        });
    });
  }

  single(options) {
    return new Promise((resolve, reject) => {
      this.model.findOne(options?.qr || {})
        .select(options?.select || {})
        .populate(options?.populate || '')
        .exec()
        .then((result) => {
          resolve(result);
        })
        .catch((e) => {
          reject(e);
        });
    });
  }

  singleUpdate(options) {
    return new Promise((resolve, reject) => {
      this.model.findOneAndUpdate(options?.qr || {}, options?.body || {})
        .select(options?.select || {})
        .populate(options?.populate || '')
        .exec()
        .then((result) => {
          resolve(result);
        })
        .catch((e) => {
          reject(e);
        });
    });
  }

  put(options) {
    const updates = {};
    Object.keys(options.body).map((item) => {
      if (options.body[item] || options.body[item] === Number(0) || options.body[item] === false) {
        updates[item] = options.body[item];
      }
      return true;
    });
    return new Promise((resolve, reject) => {
      this.model.updateOne(options ? options.params.qr : {}, {
        $set: updates
      })
        .exec()
        .then(async () => {
          const result = await this.single(options.params);
          resolve(result);
        })
        .catch((e) => {
          reject(e);
        });
    });
  }

  async delete(options) {
    const record = await this.single(options.params);
    return new Promise((resolve, reject) => {
      record.remove().then((result) => {
        resolve(result);
      }).catch((e) => {
        reject(e);
      });
    });
  }

  async deleteMany(options) {
    return new Promise((resolve, reject) => {
      this.model.deleteMany(options).then((result) => {
        resolve(result);
      }).catch((e) => {
        reject(e);
      });
    });
  }

  create(options) {
    return new Promise((resolve, reject) => {
      this.model.create(options).then((result) => {
        resolve(result);
      }).catch((err) => {
        reject(err);
      });
    });
  }
}
