import React, { createContext, useState, useMemo } from 'react';
import PropTypes from 'prop-types';
import gql from 'graphql-tag';
import Router from 'next/router';
import cookie from 'js-cookie';

import {
  useApolloClient,
  useQuery
} from '@apollo/react-hooks';

import {
  BackDrop
} from '~base';

const GET_ME = gql`
  {
    myself {
      account{
        username
        accType
        part
        status
      }
    }
  }
`;

export const SignupContext = createContext();

export const SignupProvider = ({ children }) => {
  const [user, setUser] = useState(null);

  const client = useApolloClient();

  const {
    data: { myself } = {},
    loading
  } = useQuery(GET_ME, {
    onCompleted: async ({ myself: my }) => {
      const token = cookie.get('token');
      if (!token) {
        await client.clearStore();
        Router.push('/');
      } else if (my.account.status === 'active') {
        Router.push('/auth');
      } else {
        setUser(my);
      }
    },
    onError: async () => {
      cookie.remove('token');
      await client.clearStore();
      Router.push('/');
    }
  });

  const value = useMemo(() => ({ user, setUser }), [user, setUser]);

  if (!myself) {
    return (
      <BackDrop
        open={loading || !myself}
      />
    );
  }


  return (
    <SignupContext.Provider value={value}>
      {children}
    </SignupContext.Provider>
  );
};

SignupProvider.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired
};
