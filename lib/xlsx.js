import XLSX from 'xlsx';

export default ({
  filepath,
  sheetName
}) => {
  const workbook = XLSX.readFile(
    `${process.env.publicPath}/${filepath}`,
    { cellDates: true }
  );
  const worksheet = workbook.Sheets[sheetName];
  let sheetDetail = XLSX.utils.sheet_to_row_object_array(worksheet, {
    defval: ''
  });
  sheetDetail = sheetDetail.filter((value) => JSON.stringify(value) !== '{}');
  return sheetDetail;
};
