import { createMuiTheme } from '@material-ui/core/styles';
import {
  grey
} from '@material-ui/core/colors';


const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#6573c3',
      main: '#3f51b5',
      dark: '#2c387e',
      contrastText: '#FFFFFF'
    },
    secondary: {
      light: '#ff6333',
      main: '#ff3d00',
      dark: '#b22a00',
      contrastText: '#FFF'
    },
    error: {
      light: '#e57373',
      main: '#f44336',
      dark: '#d32f2f',
      contrastText: '#FFF'
    },
    warning: {
      light: '#ffb74d',
      main: '#ff9800',
      dark: '#f57c00',
      contrastText: '#FFF'
    },
    success: {
      light: '#81c784',
      main: '#4caf50',
      dark: '#388e3c',
      contrastText: '#FFF'
    },
    info: {
      light: '#64b5f6',
      main: '#2196f3',
      dark: '#1976d2',
      contrastText: '#FFF'
    },
    background: {
      default: grey[100]
    },
    text: {
      primary: grey[800],
      secondary: grey[700]
    }
  }
  // typography: {
  //   fontFamily: [
  //     'Amar Bangla',
  //     'SutonnyMJ',
  //     'Roboto',
  //     'SutonnyOMJ',
  //     '-apple-system',
  //     'BlinkMacSystemFont',
  //     '"Segoe UI"',
  //     '"Helvetica Neue"',
  //     'Arial',
  //     'sans-serif',
  //     '"Apple Color Emoji"',
  //     '"Segoe UI Emoji"',
  //     '"Segoe UI Symbol"'
  //   ].join(','),
  //   h1: {
  //     fontFamily: 'Montserrat',
  //     fontWeight: 500,
  //     color: grey[900]
  //   },
  //   h2: {
  //     fontFamily: 'Montserrat',
  //     fontWeight: 500,
  //     color: grey[900]
  //   },
  //   h3: {
  //     fontFamily: 'Montserrat',
  //     fontWeight: 500,
  //     color: grey[900]
  //   },
  //   h4: {
  //     fontFamily: 'Montserrat',
  //     fontWeight: 500,
  //     color: grey[900]
  //   },
  //   h6: {
  //     fontFamily: 'Montserrat',
  //     fontWeight: 500,
  //     color: grey[900]
  //   },
  //   h5: {
  //     fontFamily: 'Montserrat',
  //     fontWeight: 500,
  //     color: grey[900]
  //   }
  // }
});

export default theme;
