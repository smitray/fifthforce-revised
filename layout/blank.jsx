import PropTypes from 'prop-types';
import {
  Grid,
  Paper
} from '@material-ui/core';

import { Layout } from '~style';

const blankLayout = ({
  children
}) => {
  const {
    defaultLayoutBackground
  } = Layout();

  return (
    <>
      <div
        className={defaultLayoutBackground}
      >
        <Grid
          container
          direction="column"
          justify="center"
          alignItems="center"
        >
          <Grid
            item
            xs={11}
            md={10}
          >
            <Paper
              elevation={8}
            >
              {children}
            </Paper>
          </Grid>
        </Grid>
      </div>
    </>
  );
};

blankLayout.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired
};

export default blankLayout;
