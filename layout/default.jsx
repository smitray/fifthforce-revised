import React, {
  useContext,
  useState
} from 'react';
import PropTypes from 'prop-types';
import { useQuery, useApolloClient } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import cookie from 'js-cookie';
import Router from 'next/router';

import {
  Grid as rootGrid,
  AppBar,
  withStyles
} from '@material-ui/core';
import clsx from 'clsx';

import { AuthContext } from '~lib/authContext';

import {
  AppBar as ToolBar,
  Layout as useStyles,
  Sidebar,
  NotificationSidebar
} from '~components';

const Grid = withStyles((theme) => ({
  root: {
    padding: theme.spacing(0, 3),
    margin: theme.spacing(8, 0),
    [theme.breakpoints.up('sm')]: {
      marginTop: theme.spacing(12)
    }
  }
}))(rootGrid);

const GET_ME = gql`
  {
    myself{
      name
      gender
      avatar{
        filename
        filePath
      }
      account{
        username
        email
        phone
        accType
      }
      school{
        name
        _id
      }
      student{
        _id
        class{
          _id
          standard
        }
        location{
          _id
          name
        }
        school{
          _id
          name
        }
        subjects{
          name
          _id
        }
      }
    }
  }
`;

const MainLayout = ({
  children
}) => {
  const drawerWidth = 260;

  const {
    root,
    appBar,
    gradient,
    fixedBox,
    container
  } = useStyles({
    drawerWidth
  });

  const client = useApolloClient();

  const [sidebarNav, setSidebarNav] = useState(false);
  const [notificationToggle, setNotificationToggle] = useState(false);


  const { setUser } = useContext(AuthContext);

  // useQuery(GET_ME, {
  //   onCompleted: async ({ myself }) => {
  //     const token = cookie.get('token');
  //     if (!token) {
  //       await client.clearStore();
  //       Router.push('/');
  //     }
  //     setUser(myself);
  //   },
  //   onError: async () => {
  //     cookie.remove('token');
  //     await client.clearStore();
  //     Router.push('/');
  //   }
  // });

  const toggleNavbar = () => {
    setSidebarNav(!sidebarNav);
  };

  const toggleNotification = () => {
    setNotificationToggle(!notificationToggle);
  };


  return (
    <div className={root}>
      <AppBar
        position="fixed"
        className={
          clsx(
            gradient,
            appBar
          )
        }
        elevation={0}
      >
        <ToolBar
          handleSidebar={toggleNavbar}
          handleNotification={toggleNotification}
        />
      </AppBar>
      <Sidebar
        drawerWidth={drawerWidth}
        open={sidebarNav}
        handleSidebarClose={toggleNavbar}
      />
      <div
        className={clsx(gradient, fixedBox)}
      />
      <main className={container}>
        <Grid
          container
        >
          {children}
        </Grid>
      </main>
      <NotificationSidebar
        drawerWidth={drawerWidth}
        open={notificationToggle}
        handleNotificationClose={toggleNotification}
      />
    </div>
  );
};

MainLayout.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired
};

export default MainLayout;
