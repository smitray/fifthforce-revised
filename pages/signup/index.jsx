import React from 'react';

import {
  List,
  ListItem,
  ListItemText,
  ListItemAvatar,
  Avatar,
  Icon
} from '@material-ui/core';

import Link from 'next/link';
import clsx from 'clsx';

import {
  AuthFormWrapper,
  AuthStyle,
  NotificationStyle,
  HeaderTypo
} from '~components';

import blankLayout from '../../layout/blank';

const items = [{
  id: 1,
  url: 'student',
  title: 'I am a student',
  caption: 'No documents required',
  icon: 'person',
  class: 'danger'
}, {
  id: 2,
  url: 'school',
  title: 'I am representing a school',
  caption: 'Attested copy of letterhead required',
  icon: 'school',
  class: 'success'
}, {
  id: 3,
  url: 'mentor',
  title: 'I am a mentor',
  caption: 'Photo ID is required',
  icon: 'people',
  class: 'warning'
}];

const SignupIndex = () => {
  const {
    signupIcon,
    iconSize
  } = AuthStyle();

  const {
    success,
    warning,
    danger
  } = NotificationStyle();

  return (
    <>
      <AuthFormWrapper>
        <HeaderTypo
          variant="h3"
          component="h3"
          align="center"
          gutterBottom
        >
          user type
        </HeaderTypo>
        <HeaderTypo
          variant="subtitle1"
          align="center"
          color="textSecondary"
          gutterBottom
          paragraph
        >
          Please select the appropriate user type
        </HeaderTypo>
        <List>
          {items.map((item) => (
            <Link
              key={item.id}
              href={`signup/${item.url}`}
              passHref
            >
              <ListItem
                component="a"
                button
              >
                <ListItemAvatar>
                  <Avatar
                    variant="square"
                    className={clsx(signupIcon, {
                      [danger]: item.class === 'danger',
                      [success]: item.class === 'success',
                      [warning]: item.class === 'warning'
                    })}
                  >
                    <Icon
                      className={iconSize}
                    >
                      {item.icon}
                    </Icon>
                  </Avatar>
                </ListItemAvatar>
                <ListItemText
                  primary={(
                    <HeaderTypo
                      variant="body1"
                    >
                      {item.title}
                    </HeaderTypo>
                  )}
                  secondary={item.caption}
                />
              </ListItem>
            </Link>
          ))}
        </List>
      </AuthFormWrapper>
    </>
  );
};

SignupIndex.Layout = blankLayout;

export default SignupIndex;
