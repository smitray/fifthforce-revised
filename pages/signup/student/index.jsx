import React from 'react';

import {
  AuthFormWrapper,
  CreateAccount
} from '~components';

import blankLayout from '../../../layout/blank';


const StudentSignup = () => (
  <>
    <AuthFormWrapper>
      <CreateAccount />
    </AuthFormWrapper>
  </>
);

StudentSignup.Layout = blankLayout;

export default StudentSignup;
