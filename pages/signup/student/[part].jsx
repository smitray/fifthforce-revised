import React from 'react';
import { useRouter } from 'next/router';

import {
  AuthFormWrapper,
  CreatePersonalDetails,
  AddSchool,
  SignUpContext
} from '~components';

import blankLayout from '../../../layout/blank';

const StudentPart = () => {
  const router = useRouter();
  const { part } = router.query;

  function RenderPart() {
    switch (Number(part)) {
      case 1:
        return (<CreatePersonalDetails />);
      case 2:
        return (<AddSchool />);
      default:
    }
  }

  return (
    <>
      <SignUpContext />
      <AuthFormWrapper>
        {RenderPart()}
      </AuthFormWrapper>
    </>
  );
};

StudentPart.Layout = blankLayout;

export default StudentPart;
