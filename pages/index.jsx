import {
  AuthFormWrapper,
  LoginForm
} from '~components';

import blankLayout from '../layout/blank';

const Login = () => (
  <AuthFormWrapper>
    <LoginForm />
  </AuthFormWrapper>
);
Login.Layout = blankLayout;

export default Login;
