module.exports = {
  development: {
    mongodb: {
      db: 'fifthforce-dev'
    },
    GRAPHQL_URL: 'http://localhost:3000/api/graphql'
  },
  production: {
    mongodb: {
      db: 'fifthforce-dev'
    },
    GRAPHQL_URL: 'http://142.93.214.115/api/graphql'
  },
  test: {
    mongodb: {
      db: 'fifthforce-test'
    },
    GRAPHQL_URL: 'http://localhost:3000/api/graphql'
  }
};
