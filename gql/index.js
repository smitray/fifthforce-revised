import gql from 'graphql-tag';



export const LOCATION_GET = gql`
  {
    locations{
      _id
      name
    }
  }
`;

export 

export const CREATE_SCHOOL = gql`
  mutation createSchool(
    $locationId: String!
    $name: String!
    $address: String
    $published: String
  ){
    schoolCreate(
      locationId: $locationId
      name: $name
      address: $address
      published: $published
    ){
      _id
    }
  }
`;

export const CREATE_SCHOOL_BULK = gql`
  mutation bulkSchoolCreate(
    $filepath: String!
    $filename: String
  ){
    schoolCreateFromFile(
      filepath: $filepath
      filename: $filename
    ){
      _id
    }
  }
`;

export const CREATE_STUDENT = gql`
  mutation createStudend(
    $classId: String!,
    $schoolId: String!,
    $group: String,
    $locationId: String!
  ){
    studentCreate(
      classId: $classId,
      schoolId: $schoolId,
      group: $group,
      locationId: $locationId
    ){
      _id
    }
  }
`;

export const CREATE_STUDENT_SUBJECT = gql`
  mutation createStudentSubject(
    $subjects: [subjectInput!]!
  ) {
    studentSubjectCreate(
      subjects: $subjects
    ){
      _id
    }
  }
`;

export const GET_CLASS = gql`
  {
    stClasss{
      standard
      _id
    }
  }
`;

export const CREATE_CLASS = gql`
  mutation createClass(
    $standard: Int!
  ){
    stClassCreate(
      standard: $standard
    ){
      _id
      standard
    }
  }
`;

export const DELETE_CLASS = gql`
  mutation deleteClass(
    $_id: String!
  ){
    stClassDelete(
      _id: $_id
    ){
      _id
    }
  }
`;

export const UPDATE_CLASS = gql`
  mutation updateClass(
    $_id: String!
    $standard: Int!
  ){
    stClassUpdate(
      _id: $_id
      standard: $standard
    ){
      _id
    }
  }
`;

export const SIGNUP_GQL = gql`
  mutation AccountCreate(
    $name: String!,
    $username: String,
    $email: String,
    $phone: String,
    $password: String!,
    $gender: String!,
    $accType: String
  ){
    accountCreate(
      name: $name,
      password: $password,
      username: $username,
      phone: $phone,
      email: $email,
      gender: $gender,
      accType: $accType
    ) {
      _id
      token
      accType
    }
  }
`;

export const UPDATE_USER = gql`
  mutation updateUser(
    $dob: DateTime,
    $gender: String,
    $avatar: String,
    $school: String,
    $document: String
  ){
    userUpdate(
      dob: $dob,
      gender: $gender
      avatar: $avatar
      school: $school
      document: $document
    ){
      _id
    }
  }
`;

export const GET_SUBJECTS = gql`
  query getSubjects($classId: String!){
    subjects(
      classId: $classId
    ){
      name
      _id
      group
      compulsory
    }
  }
`;

export const CREATE_SUBJECT = gql`
  mutation createSubject(
    $classId: String!
    $name: String!
    $group: String
    $compulsory: String
  ){
    subjectCreate(
      classId: $classId
      name: $name
      group: $group
      compulsory: $compulsory
    ){
      _id
    }
  }
`;

export const UPDATE_SUBJECT = gql`
  mutation subjectUpdate(
    $_id: String!
    $name: String
    $group: String
    $compulsory: String
  ){
    subjectUpdate(
      _id: $_id
      name: $name
      group: $group
      compulsory: $compulsory
    ){
      name
    }
  }
`;

export const DELETE_SUBJECT = gql`
  mutation deleteSubject(
    $_id: String!
  ){
    subjectDelete(
      _id: $_id
    ){
      _id
    }
  }
`;

export const GET_CHAPTER = gql`
  query getChapters($subjectId: String!){
    chapters(
      subjectId: $subjectId
    ) {
      name
      _id
    }
  }
`;

export const CREATE_CHAPTER = gql`
  mutation chapterCreate(
    $subjectId: String!
    $name: String!
  ){
    chapterCreate(
      subjectId: $subjectId
      name: $name
    ){
      _id
    }
  }
`;

export const UPDATE_CHAPTER = gql`
  mutation chapterUpdate(
    $_id: String!
    $name: String!
  ){
    chapterUpdate(
      _id: $_id
      name: $name
    ){
      name
    }
  }
`;

export const DELETE_CHAPTER = gql`
  mutation chapterDelete(
    $_id: String!
  ){
    chapterDelete(
      _id: $_id
    ){
      _id
    }
  }
`;

export const QUESTION_CREATE = gql`
  mutation QuestionCreate(
    $body: String!
    $qType: String!
    $imageId: String
    $subjectId: String!
    $chapterId: String!
    $questionId: String
  ) {
    questionCreate(
      body: $body
      qType: $qType
      imageId: $imageId
      subjectId: $subjectId
      chapterId: $chapterId
      questionId: $questionId
    ) {
      _id
      body
    }
  }
`;

export const ANSWER_CREATE = gql`
  mutation AnswerCreate(
    $answer: String!
    $correct: String
    $questionId: String!
  ) {
    answerCreate(
      answer: $answer, correct: $correct, questionId: $questionId
    ) {
      _id
    }
  }
`;

export const QUESTION_CREATE_BULK = gql`
  mutation QuestionBulk(
    $filename: String!
    $filepath: String!
    $subjectId: String!
    $chapterId: String!
  ) {
    questionBulk(
      filename: $filename
      filepath: $filepath
      subjectId: $subjectId
      chapterId: $chapterId
    ) {
      _id
    }
  }
`;

export const QUESTIONS_BULK_UPLOAD = gql`
  mutation questionBulkUpload(
    $filename: String!
    $filepath: String!
  ){
    questionBulkUpload(
      filename: $filename
      filepath: $filepath
    ){
      _id
    }
  }
`;

export const GET_QUESTIONS = gql`
  query getQuestions($subjectId: String!, $chapterId: String!){
    questions(
      subjectId: $subjectId
      chapterId: $chapterId
    ) {
      _id
      body
      qType
      score
      answers{
        answer
        correct
        _id
      }
    }
  }
`;

export const DELETE_QUESTION = gql`
  mutation deleteQuestion($_id: String!){
    questionDelete(_id: $_id){
      _id
    }
  }
`;

export const DELETE_ALL_QUESTION = gql`
  mutation deleteAllQuestions(
    $subjectId: String!
    $chapterId: String!
  ) {
    questionBulkDelete(
      subjectId: $subjectId
      chapterId: $chapterId
    ) {
      _id
      body
      qType
    }
  }
`;

export const UPDATE_QUESTION = gql`
  mutation updateQuestion(
    $_id: String!
    $body: String
    $score: Int
  ) {
    questionUpdate(
      _id: $_id
      body: $body
      score: $score
    ) {
      _id
      body
      score
    }
  }
`;

export const GET_QUESTION = gql`
  query getQuestion($_id: String!){
    question(
      _id: $_id
    ){
      _id
      body
      qType
      score
      file{
        _id
        filename
        filePath
      }      
      answers{
        _id
        answer
        correct
      }
      subQuestions{
        _id
        body
        score
        qType
        answers{
          _id
          answer
          correct
        }
      }
    }
  }
`;

export const GET_ANSWER = gql`
  query getAnswer($_id: String!){
    answer(
      _id: $_id
    ) {
      _id
      answer
      correct
    }
  }
`;

export const UPDATE_ANSWER = gql`
  mutation updateAnswer(
    $_id: String!
    $answer: String
    $correct: String
  ) {
    answerUpdate(
      _id: $_id
      answer: $answer
      correct: $correct
    ) {
      _id
      answer
      correct
    }
  }
`;


export const PAPER_CREATE = gql`
  mutation PaperCreate(
    $chapters: [chapterInput!]!
    $subjectId: String!
    $question: Int
    $classId: String!
    $time: Int
    $level: Int
    $activation: DateTime
  ) {
    paperCreate(
      chapters: $chapters,
      subjectId: $subjectId,
      question: $question,
      classId: $classId,
      time: $time
      level: $level
      activation: $activation
    ){
      _id
      level
      set
    }
  }
`;

export const GET_PAPERS = gql`
  query getPapers(
    $classId: String!
    $subjectId: String!
    $levelId: Int
  ){
    papers(
      classId: $classId
      subjectId: $subjectId
      levelId: $levelId
    ){
      _id
      set
      level
      class {
        standard
      }
      subject {
        name
      }
      chapter {
        name
      }
      time
      activation
      status
    }
  }
`;

export const GET_APPROVED_PAPER = gql`
  query getApprovedPapers(
    $status: String!
    $classId: String
    $subjectId: String
    $levelId: Int
  ){
    approvedPapers(
      status: $status
      classId: $classId
      subjectId: $subjectId
      levelId: $levelId
    ){
      _id
      set
      level
      class {
        standard
      }
      subject {
        name
      }
      chapter {
        name
      }
      time
      activation
      status
    }
  }
`;

export const GET_PAPER = gql`
  query getPaper(
    $_id: String!
  ){
    paper(
      _id: $_id
    ){
      set
      level
      time
      class{
        standard
        _id
      }
      subject{
        name
        _id
      }
      chapter{
        name
      }
      question{
        body
        _id
        qType
        score
        parentQts{
          body
          _id
        }
        answers{
          _id
          correct
          answer
        }
      }
    }
  }
`;

export const DELETE_PAPER = gql`
  mutation deletePaper(
    $_id: String!
  ){
    paperDelete(
      _id: $_id
    ){
      _id
    }
  }
`;

export const AUTHORIZE_PAPER = gql`
  mutation paperAuthorize(
    $_id: String!
    $status: String
    $password: String
  ){
    paperUpdate(
      _id: $_id
      status: $status
      password: $password
    ){
      _id
    }
  }
`;

export const DELETE_SYLLABUS = gql`
  mutation deleteSyllabus(
    $_id: String!
  ){
    syllabusDelete(
      _id: $_id
    ){
      _id
    }
  }
`;

export const GET_SYLLABUS = gql`
  query getSyllabus(
    $subjectId: String
    $classId: String
    $timeline: DateTime
  ){
    syllabuss(
      subjectId: $subjectId
      classId: $classId
      timeline: $timeline
    ){
      _id
      class{
        _id
        standard
      }
      sub{
        _id
        name
      }
      stClass
      subject
      chapters
      activation
      timeline
      question
      time
      chapter{
        _id
        name
      }
    }
  }
`;

export const CREATE_SYLLABUS = gql`
  mutation createSyllabus(
    $filepath: String!
  ){
    syllabusCreate(
      filepath: $filepath
    ){
      _id
    }
  }
`;

export const BULK_GENERATOR = gql`
  mutation generateBulk(
    $filename: String
    $filepath: String!
  ){
    bulkGenerator(
      filename: $filename
      filepath: $filepath
    ){
      _id
    }
  }
`;

export const EXAM_CREATE = gql`
  query createExam(
    $subjectId: String!
    $classId: String!
  ){
    exams(
      subjectId: $subjectId
      classId: $classId
    ){
      _id
      paper{
        _id
        time
        chapter{
          name
        }
      }
      attempt
      level
      status
      score
      range
      timeline
      result{
        paper{
          _id
          level
          set
        }
      }
    }
  }
`;

export const CREATE_ANSWER = gql`
  mutation createResult(
    $paperId: String!
    $questionId: String!
    $answerCorrectId: String!
    $answerGivenId: String!
    $timeTaken: Int,
    $score: Int!
  ){
    resultCreate(
      paperId: $paperId
      questionId: $questionId
      answerCorrectId: $answerCorrectId
      answerGivenId: $answerGivenId
      timeTaken: $timeTaken,
      score: $score
    ){
      _id
    }
  }
`;

export const RESULT_GROUP_CREATE = gql`
  mutation resultGroupCreate(
    $examId: String!
    $paperId: String!
  ){
    resultsGroupCreate(
      examId: $examId
      paperId: $paperId
    ){
      _id
    }
  }
`;

export const UPDATE_EXAM = gql`
  mutation examUpdate(
    $_id: String!
  ){
    examUpdate(
      _id: $_id
    ){
      _id
    }
  }
`;

export const GET_RESULTS = gql`
  query getResults(
    $paperId: String!
  ){
    results(
      paperId: $paperId
    ){
      _id
      question{
        _id
        body
        qType
        parentQts{
          body
        }
      }
      answerGiven{
        _id
        answer
      }
      answerCorrect{
        _id
        answer
      }
      score
      timeTaken
    }
  }
`;

export const GET_SUBJECT_RESULTS = gql`
  query getSubjuectResults(
    $subjectId: String!
    $classId: String!
  ){
    completedExams(
      subjectId: $subjectId
      classId: $classId
    ){
      _id
      paper{
        _id
        level
        set
        subject{
          name
        }
        chapter{
          name
        }
      }
      timeline
    }
  }
`;
