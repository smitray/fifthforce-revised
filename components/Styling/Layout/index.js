import {
  makeStyles
} from '@material-ui/core';

export default makeStyles((theme) => ({
  root: {
    display: 'flex'
  },
  appBar: {
    [theme.breakpoints.up('sm')]: {
      width: ({ drawerWidth }) => `calc(100% - ${drawerWidth}px)`,
      marginLeft: ({ drawerWidth }) => drawerWidth
    }
  },
  gradient: {
    background: 'linear-gradient(to right, #0083B0, #00B4DB)'
  },
  fixedBox: {
    position: 'absolute',
    zIndex: '-1',
    top: 0,
    left: ({ drawerWidth }) => drawerWidth,
    width: ({ drawerWidth }) => `calc(100% - ${drawerWidth}px)`,
    height: 200,
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block'
    }
  },
  container: {
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: ({ drawerWidth }) => `calc(100% - ${drawerWidth}px)`
    }
  },
  grow: {
    flexGrow: 1
  },
  menuButton: {
    marginRight: theme.spacing(1),
    [theme.breakpoints.up('sm')]: {
      display: 'none'
    }
  },
  appBarLogo: {
    [theme.breakpoints.up('sm')]: {
      display: 'none'
    }
  },
  defaultLayoutBackground: {
    minHeight: '100vh',
    background: 'linear-gradient(to right, #0083B0, #00B4DB)',
    padding: theme.spacing(10, 0)
  }
}));
