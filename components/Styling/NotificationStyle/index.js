import {
  makeStyles
} from '@material-ui/core';

export default makeStyles((theme) => ({
  success: {
    color: theme.palette.success.contrastText,
    backgroundColor: theme.palette.success.main
  },
  danger: {
    color: theme.palette.error.contrastText,
    backgroundColor: theme.palette.error.main
  },
  warning: {
    color: theme.palette.warning.contrastText,
    backgroundColor: theme.palette.warning.main
  },
  info: {
    color: theme.palette.info.contrastText,
    backgroundColor: theme.palette.info.main
  },
  listStyle: {
    background: theme.palette.background.default,
    marginBottom: 4
  }
}));
