import {
  withStyles,
  Badge
} from '@material-ui/core';


export default withStyles((theme) => ({
  badge: {
    background: `radial-gradient(circle, ${theme.palette.warning.dark} 0%, ${theme.palette.warning.main} 100%)`,
    color: theme.palette.warning.contrastText,
    boxShadow: '0 0 10px 0 #f9daac'
  }
}))(Badge);
