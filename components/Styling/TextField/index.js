import {
  withStyles,
  TextField
} from '@material-ui/core';

export default withStyles((theme) => ({
  root: {
    margin: theme.spacing(2, 0, 4)
  }
}))(TextField);
