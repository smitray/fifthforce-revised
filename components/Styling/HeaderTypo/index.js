import {
  withStyles,
  Typography
} from '@material-ui/core';


export default withStyles(() => ({
  h1: {
    fontFamily: 'Montserrat',
    fontWeight: 700
  },
  h2: {
    fontFamily: 'Montserrat',
    fontWeight: 700
  },
  h3: {
    fontFamily: 'Montserrat',
    fontWeight: 600,
    textTransform: 'uppercase',
    fontSize: '1.8rem'
  },
  h4: {
    fontFamily: 'Montserrat',
    fontWeight: 700
  },
  h5: {
    fontFamily: 'Montserrat',
    fontWeight: 700
  },
  h6: {
    fontFamily: 'Montserrat',
    fontWeight: 700
  },
  body1: {
    fontFamily: 'Montserrat',
    fontWeight: 700
  }
}))(Typography);
