import {
  withStyles,
  Typography
} from '@material-ui/core';


export default withStyles(() => ({
  button: {
    fontWeight: 700,
    color: '#000'
  },
  subtitle1: {
    textTransform: 'capitalize',
    fontWeight: 500
  }
}))(Typography);
