import {
  withStyles,
  Divider
} from '@material-ui/core';


export default withStyles((theme) => ({
  root: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(2)
  }
}))(Divider);
