import {
  makeStyles
} from '@material-ui/core';

export default makeStyles((theme) => ({
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: ({ drawerWidth }) => drawerWidth,
      flexShrink: 0
    }
  },
  drawerPaper: {
    width: ({ drawerWidth }) => drawerWidth
  },
  avatar: {
    width: 100,
    height: 100,
    margin: theme.spacing(3, 0, 2),
    color: theme.palette.warning.contrastText,
    backgroundColor: theme.palette.warning.main
  },
  navigationContainer: {
    width: '92%',
    color: 'rgba(0, 0, 0, .87)'
  },
  activeColor: {
    color: '#fff',
    background: 'linear-gradient(to right, #0083B0, #00B4DB)',
    borderRadius: '0px 5px 5px 0px',
    paddingTop: 4,
    paddingBottom: 4,
    boxShadow: '3px 3px 20px 0 rgba(17,117,208,.5)',
    '& .MuiIcon-root': {
      color: '#fff'
    }
  },
  listItemTypo: {
    '& span': {
      fontFamily: 'Montserrat',
      fontSize: '0.85rem',
      fontWeight: 500
    }
  },
  activeBackGround: {
    background: 'rgba(237, 237, 237, 0.4)',
    '& + .MuiCollapse-container': {
      background: 'rgba(237, 237, 237, 0.4)'
    }
  },
  arrowColor: {
    color: 'rgb(175, 175, 175)'
  },
  icon: {
    fontSize: '1.3rem',
    marginTop: 3
  },
  iconDefault: {
    fontSize: '0.8rem',
    marginTop: 3,
    marginLeft: 4
  },
  notificationWr: {
    width: '100%',
    padding: theme.spacing(2, 1)
  },
  hideLogo: {
    [theme.breakpoints.down('xs')]: {
      display: 'none'
    }
  }
}));
