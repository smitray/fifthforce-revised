import {
  withStyles,
  Typography
} from '@material-ui/core';


export default withStyles((theme) => ({
  subtitle1: {
    fontFamily: 'Montserrat',
    fontWeight: 700,
    textDecoration: 'none',
    color: 'inherit',
    fontSize: '1.2rem'
  },
  caption: {
    fontFamily: 'Montserrat',
    fontWeight: 800,
    textDecoration: 'none',
    fontSize: '1.7rem',
    textAlign: 'center',
    display: 'block',
    marginTop: theme.spacing(3),
    textShadow: '2px 2px 2px rgba(63, 81, 181, 0.3)',
    color: theme.palette.primary.dark
  }
}))(Typography);
