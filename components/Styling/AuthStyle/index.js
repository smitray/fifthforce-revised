import {
  makeStyles
} from '@material-ui/core';

export default makeStyles((theme) => ({
  leftBlock: {
    padding: theme.spacing(6),
    background: '#f4f7ff',
    '& img': {
      width: '100%',
      margin: theme.spacing(3, 0)
    }
  },
  rightBlock: {
    padding: theme.spacing(9, 10)
  },
  logoStyle: {
    fontWeight: '800 !important',
    fontSize: '2.125rem !important',
    marginTop: '0 !important'
  },
  submitBtn: {
    padding: theme.spacing(1, 3)
  },
  forgotLink: {
    textTransform: 'capitalize',
    textDecoration: 'none',
    color: theme.palette.primary.main,
    '&:hover': {
      color: theme.palette.primary.dark
    }
  },
  createLink: {
    textTransform: 'capitalize',
    textDecoration: 'none',
    display: 'block',
    color: 'inherit',
    '&:hover': {
      color: theme.palette.primary.dark
    }
  },
  dividerStyle: {
    margin: theme.spacing(3, 0, 2)
  },
  signupIcon: {
    width: theme.spacing(8),
    height: theme.spacing(8),
    marginRight: theme.spacing(2)
  },
  iconSize: {
    fontSize: '2rem'
  },
  dobStyle: {
    margin: theme.spacing(2, 0, 4)
  },
  addSchoolBtn: {
    margin: theme.spacing(1, 0, 2)
  }
}));
