import React from 'react';
import PropTypes from 'prop-types';

import {
  Drawer,
  List
} from '@material-ui/core';

import {
  SidebarStyle
} from '~style';

import {
  Notification
} from '~elements';

const data = [{
  _id: 1,
  content: 'You did not clear your exam',
  nType: 'danger',
  url: '/'
}, {
  _id: 2,
  content: 'XYZ added you, please verify',
  nType: 'info',
  url: '/'
}, {
  _id: 3,
  content: 'You cleared your exam successfully',
  nType: 'success',
  url: '/'
}, {
  _id: 4,
  content: 'You have a pending exam',
  nType: 'warning',
  url: '/'
}];

const NotificationSidebar = ({
  drawerWidth,
  open,
  handleNotificationClose
}) => {
  const {
    drawer,
    drawerPaper,
    notificationWr
  } = SidebarStyle({
    drawerWidth
  });

  return (
    <>
      <aside
        className={drawer}
      >
        <Drawer
          open={open}
          anchor="right"
          onClose={handleNotificationClose}
          classes={{
            paper: drawerPaper
          }}
        >
          <List
            className={notificationWr}
            onClick={handleNotificationClose}
          >
            <Notification
              data={data}
            />
          </List>
        </Drawer>
      </aside>
    </>
  );
};

NotificationSidebar.propTypes = {
  drawerWidth: PropTypes.number.isRequired,
  open: PropTypes.bool.isRequired,
  handleNotificationClose: PropTypes.func.isRequired
};

export default NotificationSidebar;
