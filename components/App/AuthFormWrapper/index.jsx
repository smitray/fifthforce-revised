import React from 'react';
import PropTypes from 'prop-types';

import {
  Grid, Typography
} from '@material-ui/core';

import Link from 'next/link';

import {
  AuthStyle,
  LogoStyling as Logo
} from '~style';

const AuthFormWrapper = ({
  children
}) => {
  const {
    leftBlock,
    rightBlock,
    logoStyle
  } = AuthStyle();

  return (
    <>
      <Grid
        container
      >
        <Grid
          item
          xs={12}
          md={6}
          className={leftBlock}
        >
          <Link
            href="/"
            passHref
          >
            <Logo
              variant="caption"
              component="a"
              className={logoStyle}
            >
              FifthForce
            </Logo>
          </Link>
          <img
            src="/bg-img.png"
            alt="Fifth Force background"
          />
          <Typography
            variant="subtitle1"
            align="center"
            gutterBottom
            paragraph
          >
            Lorem ipsum dolor sit amet consectetur, adipisicing elit.
            Quis dicta quos amet velit nihil pariatur.
          </Typography>
        </Grid>
        <Grid
          item
          xs={12}
          md={6}
          className={rightBlock}
        >
          {children}
        </Grid>
      </Grid>
    </>
  );
};

AuthFormWrapper.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired
};

export default AuthFormWrapper;
