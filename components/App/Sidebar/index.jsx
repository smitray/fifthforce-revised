import React, {
  useContext
} from 'react';

import PropTypes from 'prop-types';

import {
  Drawer,
  useMediaQuery,
  useTheme
} from '@material-ui/core';

import Link from 'next/link';

import { AuthContext } from '~lib/authContext';

import {
  SidebarStyle as useStyles,
  LogoStyling as Logo
} from '~style';

import {
  NavigationContainer
} from '~base';

import {
  UserInfoSidebar
} from '~elements';

const Sidebar = ({
  drawerWidth,
  open,
  handleSidebarClose
}) => {
  const {
    drawer,
    drawerPaper,
    hideLogo
  } = useStyles({
    drawerWidth
  });
  const theme = useTheme();
  const { user } = useContext(AuthContext);

  return (
    <>
      <nav className={drawer}>
        <Drawer
          variant={useMediaQuery(theme.breakpoints.up('sm')) ? 'permanent' : 'temporary'}
          anchor="left"
          open={useMediaQuery(theme.breakpoints.up('sm')) ? true : open}
          onClose={handleSidebarClose}
          classes={{
            paper: drawerPaper
          }}
          ModalProps={{
            keepMounted: true
          }}
          PaperProps={{
            elevation: 10
          }}
        >
          <Link
            href="/"
            passHref
          >
            <Logo
              variant="caption"
              component="a"
              className={hideLogo}
            >
              FifthForce
            </Logo>
          </Link>
          <UserInfoSidebar
            user={{
              name: 'Debasmit Ray',
              account: {
                accType: 'student'
              }
            }}
          />
          <NavigationContainer />
        </Drawer>
      </nav>
    </>
  );
};

Sidebar.propTypes = {
  drawerWidth: PropTypes.number,
  open: PropTypes.bool,
  handleSidebarClose: PropTypes.func.isRequired
};

Sidebar.defaultProps = {
  drawerWidth: 260,
  open: false
};

export default Sidebar;
