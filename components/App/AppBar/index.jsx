import React, {
  useState
} from 'react';

import PropTypes from 'prop-types';

import {
  NotificationsOutlined,
  AccountCircleOutlined,
  Menu
} from '@material-ui/icons';

import {
  Toolbar,
  MenuItem,
  Popover,
  Divider
} from '@material-ui/core';

import Link from 'next/link';

import {
  IconButton
} from '~base';

import {
  Layout as useStyles,
  LogoStyling as Logo,
  BadgeStyling as Badge
} from '~style';

const AppBar = ({
  handleSidebar,
  handleNotification
}) => {
  const {
    grow,
    menuButton,
    appBarLogo
  } = useStyles();

  const [profileEl, setProfileEl] = useState(null);
  const openProfile = Boolean(profileEl);

  const handleProfileMenu = (event) => {
    setProfileEl(event.currentTarget);
  };

  const handleClose = () => {
    setProfileEl(null);
  };

  return (
    <>
      <Toolbar>
        <IconButton
          color="inherit"
          aria-label="open drawer"
          edge="start"
          className={menuButton}
          onClick={handleSidebar}
        >
          <Menu />
        </IconButton>
        <div className={grow}>
          <Link
            href="/"
            passHref
          >
            <Logo
              variant="subtitle1"
              component="a"
              className={appBarLogo}
            >
              FifthForce
            </Logo>
          </Link>
        </div>
        <div>
          <IconButton
            aria-label="account of current user"
            color="inherit"
            aria-controls="profile-dd"
            onClick={handleProfileMenu}
            aria-haspopup="true"
          >
            <AccountCircleOutlined />
          </IconButton>
          <IconButton
            aria-label="show 5 new notifications"
            color="inherit"
            aria-controls="notification-dd"
            onClick={handleNotification}
            aria-haspopup="true"
          >
            <Badge
              badgeContent={4}
            >
              <NotificationsOutlined />
            </Badge>
          </IconButton>
          <Popover
            anchorEl={profileEl}
            keepMounted
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'right'
            }}
            transformOrigin={{
              vertical: 'top',
              horizontal: 'right'
            }}
            open={openProfile}
            onClose={handleClose}
          >
            <MenuItem onClick={handleClose}>Profile</MenuItem>
            <MenuItem onClick={handleClose}>My account</MenuItem>
            <Divider />
            <MenuItem onClick={handleClose}>Sign Out</MenuItem>
          </Popover>
        </div>
      </Toolbar>
    </>
  );
};

AppBar.propTypes = {
  handleSidebar: PropTypes.func.isRequired,
  handleNotification: PropTypes.func.isRequired
};

export default AppBar;
