import React, {
  useState
} from 'react';
import PropTypes from 'prop-types';
import DateFnsUtils from '@date-io/date-fns';

import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from '@material-ui/pickers';

const DatePicker = ({
  cbDate,
  ...other
}) => {
  const [selectedDate, setSelectedDate] = useState(new Date());

  const handleDateChange = (date) => {
    setSelectedDate(date);
    cbDate(date);
  };

  /**
   * Month and Year: 'MMMM, yyyy'
   * Date: 'dd/MM/yyyy'
   */

  return (
    <>
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <KeyboardDatePicker
          inputVariant="outlined"
          id="date-picker-dialog"
          value={selectedDate}
          onChange={handleDateChange}
          KeyboardButtonProps={{
            'aria-label': 'change date'
          }}
          {...other}
        />
      </MuiPickersUtilsProvider>
    </>
  );
};

DatePicker.propTypes = {
  cbDate: PropTypes.func.isRequired
};

export default DatePicker;
