import React from 'react';
import PropTypes from 'prop-types';
import {
  Autocomplete
} from '@material-ui/lab';

import { TextField } from '~style';

const AutoComplete = ({
  data,
  dataKey,
  dataCallBack,
  label,
  error
}) => (
  <>
    <Autocomplete
      options={data}
      disabled={!data}
      getOptionLabel={(option) => option[dataKey]}
      disableClearable
      onChange={dataCallBack}
      renderInput={(params) => (
        <TextField
          {...params}
          label={label}
          variant="outlined"
          fullWidth
          error={error}
        />
      )}
    />
  </>
);

AutoComplete.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  dataKey: PropTypes.string.isRequired,
  dataCallBack: PropTypes.func.isRequired,
  label: PropTypes.string.isRequired,
  error: PropTypes.bool
};

AutoComplete.defaultProps = {
  error: false
};

export default AutoComplete;
