import React from 'react';
import PropTypes from 'prop-types';
import {
  Dialog,
  DialogTitle,
  DialogActions,
  DialogContent,
  DialogContentText,
  Divider
} from '@material-ui/core';

const ModalBox = ({
  actions,
  title,
  textContent,
  children,
  ...other
}) => (
  <>
    <Dialog
      {...other}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
      <Divider />
      <DialogContent>
        {textContent && (
          <DialogContentText id="alert-dialog-description">
            {textContent}
          </DialogContentText>
        )}
        {children}
      </DialogContent>
      {actions && (
      <DialogActions>
        {actions}
      </DialogActions>
      )}
    </Dialog>
  </>
);

ModalBox.propTypes = {
  actions: PropTypes.node,
  title: PropTypes.string.isRequired,
  textContent: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired
};

ModalBox.defaultProps = {
  actions: '',
  textContent: null
};

export default ModalBox;
