import React from 'react';
import PropTypes from 'prop-types';
import {
  withStyles,
  Table,
  TableBody,
  TableCell as RootCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  Checkbox
} from '@material-ui/core';

const TableCell = withStyles((theme) => ({
  head: {
    color: theme.palette.grey[700]
  }
}))(RootCell);

const CTable = ({
  heads,
  data,
  isToolbar,
  handleSelectAll,
  isPagination,
  rowsPerPage,
  page,
  onChangePage,
  onChangeRowsPerPage,
  children
}) => (
  <>
    <TableContainer>
      <Table>
        <TableHead>
          <TableRow>
            {isToolbar && (
              <TableCell padding="checkbox">
                <Checkbox
                  onChange={handleSelectAll}
                />
              </TableCell>
            )}
            {heads.map((head) => (
              <TableCell
                align={head.align}
                key={head.id}
              >
                {head.label}
              </TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {children}
        </TableBody>
      </Table>
    </TableContainer>
    {isPagination && (
      <TablePagination
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={data.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={onChangePage}
        onChangeRowsPerPage={onChangeRowsPerPage}
      />
    )}
  </>
);

CTable.propTypes = {
  heads: PropTypes.arrayOf(PropTypes.object).isRequired,
  isToolbar: PropTypes.bool,
  isPagination: PropTypes.bool,
  handleSelectAll: PropTypes.func,
  data: PropTypes.arrayOf(PropTypes.object),
  rowsPerPage: PropTypes.number,
  page: PropTypes.number,
  onChangePage: PropTypes.func,
  onChangeRowsPerPage: PropTypes.func,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired
};

CTable.defaultProps = {
  isToolbar: false,
  isPagination: false,
  handleSelectAll: () => {},
  data: null,
  rowsPerPage: 0,
  page: 0,
  onChangePage: () => {},
  onChangeRowsPerPage: () => {}
};

export default CTable;
