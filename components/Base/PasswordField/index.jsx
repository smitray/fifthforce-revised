import React, {
  useState
} from 'react';
import PropTypes from 'prop-types';

import {
  InputAdornment
} from '@material-ui/core';

import {
  Visibility,
  VisibilityOff
} from '@material-ui/icons';

import IconButton from '../IconButton';

import {
  TextField
} from '~style';

const PasswordField = ({
  register,
  error
}) => {
  const [show, setShow] = useState(false);

  return (
    <>
      <TextField
        label="Password"
        type={show ? 'text' : 'password'}
        variant="outlined"
        fullWidth
        autoComplete="password"
        name="password"
        error={error}
        inputRef={register}
        placeholder="Password"
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
              <IconButton
                aria-label="toggle password visibility"
                onClick={() => {
                  setShow(!show);
                }}
                edge="end"
                color="default"
              >
                {show ? <Visibility /> : <VisibilityOff />}
              </IconButton>
            </InputAdornment>
          )
        }}
      />
    </>
  );
};

PasswordField.propTypes = {
  register: PropTypes.func.isRequired,
  error: PropTypes.bool
};

PasswordField.defaultProps = {
  error: false
};

export default PasswordField;
