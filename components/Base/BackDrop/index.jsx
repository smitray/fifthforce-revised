import React from 'react';
import {
  Backdrop as RootBackDrop,
  CircularProgress,
  withStyles
} from '@material-ui/core';

const Backdrop = withStyles((theme) => ({
  root: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff'
  }
}))(RootBackDrop);

const BackDrop = ({
  ...other
}) => (
  <>
    <Backdrop
      {...other}
    >
      <CircularProgress color="inherit" />
    </Backdrop>
  </>
);

export default BackDrop;
