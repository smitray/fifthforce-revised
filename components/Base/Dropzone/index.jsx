import React, { useCallback, useMemo } from 'react';
import PropTypes from 'prop-types';
import { useMutation } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import { useDropzone } from 'react-dropzone';

const SINGLE_UPLOAD_MUTATION = gql`
  mutation multipleFileCreate(
    $files: [Upload!]!
    $temp: Boolean
  ) {
    multipleFileCreate(
      files: $files
      temp: $temp
    ) {
      _id
      filename
      filePath
      createdAt
    }
  }
`;

const baseStyle = (height) => ({
  flex: 1,
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  minHeight: `${height}px`,
  borderWidth: 2,
  borderRadius: 2,
  borderColor: '#eeeeee',
  borderStyle: 'dashed',
  backgroundColor: '#fafafa',
  color: '#bdbdbd',
  outline: 'none',
  transition: 'border .24s ease-in-out',
  justifyContent: 'center'
});

const activeStyle = {
  borderColor: '#2196f3'
};

const rejectStyle = {
  borderColor: '#ff1744'
};

const Dropzone = ({
  fileDetail,
  fileType,
  maxSize,
  height,
  multiple,
  temp
}) => {
  const [uploadFileMutation] = useMutation(SINGLE_UPLOAD_MUTATION);
  const onDrop = useCallback(async (files) => {
    try {
      const { data } = await uploadFileMutation({
        variables: {
          files,
          temp
        }
      });
      fileDetail(data);
    } catch (error) {
      throw new Error(error);
    }
  }, []);

  const {
    getRootProps,
    getInputProps,
    isDragActive,
    rejectedFiles
  } = useDropzone({
    onDrop,
    accept: fileType,
    maxSize,
    multiple
  });

  const isFileTooLarge = rejectedFiles.length > 0 && rejectedFiles[0].size > maxSize;
  const style = useMemo(() => ({
    ...baseStyle(height),
    ...(isDragActive ? activeStyle : {}),
    ...(isFileTooLarge ? rejectStyle : {})
  }), [isDragActive, isFileTooLarge]);

  return (
    <>
      <div {...getRootProps({ style })}>
        <input {...getInputProps()} />
        <p>Drag &apos;n&apos; drop some files here, or click to select files</p>
      </div>
    </>
  );
};

Dropzone.propTypes = {
  /**
   * File is defined to provide the file details
   */
  fileDetail: PropTypes.func.isRequired,
  /**
   * FileType is defined to provide mime types
   */
  fileType: PropTypes.string.isRequired,
  /**
   * MaxSize is defined to provide the maximum file size
   */
  maxSize: PropTypes.number,
  /**
   * Height is defined to provide the height of the box
   */
  height: PropTypes.number,
  /**
   * Multiple is defined to accept multiple files
   */
  multiple: PropTypes.bool,
  temp: PropTypes.bool
};

Dropzone.defaultProps = {
  maxSize: 104857600,
  height: 240,
  multiple: false,
  temp: false
};

export default Dropzone;
