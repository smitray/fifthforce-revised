export default [
  {
    id: 100,
    label: 'Dashboard',
    url: '/',
    iconName: 'dvr',
    role: [
      'admin',
      'student',
      'mentor',
      'school'
    ]
  }, {
    id: 200,
    label: 'Paper',
    url: '/exam',
    iconName: 'spellcheck',
    role: [
      'student'
    ]
  }, {
    id: 300,
    label: 'Result',
    iconName: 'assignment',
    role: [
      'student'
    ],
    items: [{
      id: 301,
      label: 'Present',
      url: '/result'
    }, {
      id: 302,
      label: 'Past',
      url: '/result'
    }]
  }
];
