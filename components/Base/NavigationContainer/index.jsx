import React, {
  Fragment
} from 'react';

import {
  List
} from '@material-ui/core';

import { SidebarStyle } from '~style';

import { Navigation } from '~elements';

import items from './navigation';

const NavigationContainer = () => {
  const {
    navigationContainer
  } = SidebarStyle();

  return (
    <>
      <List component="nav" disablePadding className={navigationContainer}>
        {items.map((sidebarItem) => (
          <Fragment key={`${sidebarItem.name}-${sidebarItem.id}`}>
            <Navigation item={sidebarItem} />
          </Fragment>
        ))}
      </List>
    </>
  );
};


export default NavigationContainer;
