import React, {
  useEffect
} from 'react';

import {
  useMutation,
  useApolloClient
} from '@apollo/react-hooks';

import { useRouter } from 'next/router';
import cookie from 'js-cookie';
import * as yup from 'yup';
import gql from 'graphql-tag';

import Link from 'next/link';

import {
  useForm
} from 'react-hook-form';

import {
  Alert
} from '@material-ui/lab';

import {
  Grid,
  Typography,
  Divider
} from '@material-ui/core';

import {
  HeaderTypo,
  TextField,
  AuthStyle
} from '~style';

import {
  PasswordField,
  Button
} from '~base';

const schema = yup.object().shape({
  username: yup
    .string()
    .required('Username / Phone / Email is required'),
  password: yup
    .string()
    .required('Password is required')
    .min(8, 'Password should contain minimum 8 characters')
});

const LOGIN = gql`
  mutation AccountLogin($cred: String!, $password: String!){
    accountLogin(cred: $cred, password: $password){
      _id
      token
    }
  }
`;

const LoginForm = () => {
  const {
    submitBtn,
    forgotLink,
    createLink,
    dividerStyle
  } = AuthStyle();
  const client = useApolloClient();
  const router = useRouter();

  const {
    register,
    handleSubmit,
    errors
  } = useForm({
    validationSchema: schema
  });

  const [login, {
    error
  }] = useMutation(
    LOGIN
  );

  useEffect(() => {
    const token = cookie.get('token');
    if (token) {
      router.push('/auth');
    }
  }, []);

  const handleLogin = async ({ username, password }) => {
    await client.resetStore();
    const { data: { accountLogin } } = await login({
      variables: {
        cred: username,
        password
      }
    });
    cookie.set('token', accountLogin.token, { expires: 1 });
    router.push('/auth');
  };

  return (
    <>
      <HeaderTypo
        variant="h3"
        component="h3"
        align="center"
        gutterBottom
      >
        Login
      </HeaderTypo>
      <HeaderTypo
        variant="subtitle1"
        align="center"
        color="textSecondary"
        gutterBottom
        paragraph
      >
        Sign into your account
      </HeaderTypo>
      <form onSubmit={handleSubmit(handleLogin)}>
        {error && (
          <Alert severity="error">{error.message.replace('GraphQL error:', '').trim()}</Alert>
        )}
        <TextField
          label="Email / Username / Phone No."
          variant="outlined"
          fullWidth
          autoComplete="email"
          name="username"
          error={!!errors.username}
          inputRef={register}
          placeholder="Email / Username / Phone No"
        />
        {errors.username && (
          <Alert
            severity="error"
          >
            {errors.username.message}
          </Alert>
        )}
        <PasswordField
          error={!!errors.password}
          register={register}
        />
        {errors.password && (
          <Alert
            severity="error"
          >
            {errors.password.message}
          </Alert>
        )}
        <Grid
          container
          alignItems="center"
          justify="space-between"
        >
          <Button
            color="primary"
            className={submitBtn}
            type="submit"
          >
            login
          </Button>
          <Link
            href="/forgot"
            passHref
          >
            <Typography
              component="a"
              variant="button"
              className={forgotLink}
            >
              Forgot password?
            </Typography>
          </Link>
        </Grid>
        <Divider
          className={dividerStyle}
        />
        <Link
          href="/signup"
          passHref
        >
          <Typography
            component="a"
            variant="button"
            align="center"
            className={createLink}
          >
            Don&apos;t have an account? Create your free account
          </Typography>
        </Link>
      </form>
    </>
  );
};

export default LoginForm;
