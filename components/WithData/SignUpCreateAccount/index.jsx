import React, {
  useState
} from 'react';

import PropTypes from 'prop-types';
import { useRouter } from 'next/router';
import cookie from 'js-cookie';
import gql from 'graphql-tag';
import * as yup from 'yup';

import {
  useQuery,
  useMutation,
  useApolloClient
} from '@apollo/react-hooks';

import {
  Alert
} from '@material-ui/lab';

import {
  useForm
} from 'react-hook-form';

import {
  TextField,
  AuthStyle,
  HeaderTypo
} from '~style';

import {
  Button,
  PasswordField
} from '~base';

const SIGNUP = gql`
  mutation AccountCreate(
    $username: String!
    $email: String
    $phone: String!
    $password: String!
    $accType: String
  ){
    accountCreate(
      password: $password
      username: $username
      phone: $phone
      email: $email
      accType: $accType
    ) {
      _id
      token
      accType
    }
  }
`;

const EMAIL_VERIFY = gql`
  query vEmail(
    $email: String!
  ){
    emailV(
      email: $email
      ){
        message
        success
    }
  }
`;
const PHONE_VERIFY = gql`
  query vPhone(
    $phone: String!
  ){
    phoneV(
      phone: $phone
      ){
        message
        success
    }
  }
`;
const USERNAME_VERIFY = gql`
  query vEmail(
    $username: String!
  ){
    usernameV(
      username: $username
      ){
        message
        success
    }
  }
`;

const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;


const schema = yup.object().shape({
  email: yup
    .string()
    .email('Please provide corect email format'),
  username: yup
    .string()
    .required('Username is required')
    .min(5, 'Username must contain minimum 5 characters'),
  phone: yup
    .string()
    .required('Phone number is required')
    .matches(phoneRegExp, 'Phone number is not valid'),
  password: yup
    .string()
    .required('Password is required')
    .min(8, 'Password must contain 8 characters')
});


const CreateAccount = ({
  type
}) => {
  const {
    submitBtn
  } = AuthStyle();

  const [emailV, setEmailV] = useState(null);
  const [phoneV, setPhoneV] = useState(null);
  const [usernameV, setUsernameV] = useState(null);
  const [sbmt, setSbmt] = useState(true);

  const client = useApolloClient();
  const router = useRouter();

  const [signUp, {
    error
  }] = useMutation(
    SIGNUP
  );

  const {
    register,
    handleSubmit,
    errors,
    setError,
    clearError
  } = useForm({
    validationSchema: schema
  });

  useQuery(EMAIL_VERIFY, {
    variables: {
      email: emailV
    },
    skip: !emailV,
    onCompleted: (data) => {
      if (data?.emailV.success) {
        setError('email', 'required', 'This email address is already used');
        setSbmt(false);
      } else {
        clearError('email');
        setSbmt(true);
      }
    }
  });
  useQuery(PHONE_VERIFY, {
    variables: {
      phone: phoneV
    },
    skip: !phoneV,
    onCompleted: (data) => {
      if (data?.phoneV.success) {
        setError('phone', 'required', 'This phone number is already used');
        setSbmt(false);
      } else {
        clearError('phone');
        setSbmt(true);
      }
    }
  });
  useQuery(USERNAME_VERIFY, {
    variables: {
      username: usernameV
    },
    skip: !usernameV,
    onCompleted: (data) => {
      if (data?.usernameV.success) {
        setError('username', 'required', 'This username is already used');
        setSbmt(false);
      } else {
        clearError('username');
        setSbmt(true);
      }
    }
  });


  const handleVerifyEmail = ({ target: { value } }) => {
    setEmailV(value);
  };
  const handleVerifyPhone = ({ target: { value } }) => {
    setPhoneV(value);
  };
  const handleVerifyUsername = ({ target: { value } }) => {
    setUsernameV(value);
  };

  const handleSignUp = async (input) => {
    await client.resetStore();
    const { data: { accountCreate } } = await signUp({
      variables: {
        ...input,
        accType: type
      }
    });
    cookie.set('token', accountCreate.token, { expires: 1 });
    router.push(`/signup/${type}/1`);
  };

  return (
    <>
      <HeaderTypo
        variant="h3"
        component="h3"
        align="center"
        gutterBottom
      >
        signup
      </HeaderTypo>
      <HeaderTypo
        variant="subtitle1"
        align="center"
        color="textSecondary"
        gutterBottom
        paragraph
      >
        Create an account
      </HeaderTypo>
      <form onSubmit={handleSubmit(handleSignUp)}>
        {error && (
          <Alert
            severity="error"
          >
            {error.message.replace('GraphQL error:', '').trim()}
          </Alert>
        )}
        <TextField
          label="Username *"
          variant="outlined"
          fullWidth
          name="username"
          autoComplete="username"
          error={!!errors.username}
          inputRef={register}
          placeholder="Username"
          onBlur={handleVerifyUsername}
        />
        {errors.username && (
          <Alert
            severity="error"
          >
            {errors.username.message}
          </Alert>
        )}
        <TextField
          label="Email"
          variant="outlined"
          fullWidth
          type="email"
          name="email"
          autoComplete="email"
          error={!!errors.email}
          inputRef={register}
          placeholder="Email"
          onBlur={handleVerifyEmail}
        />
        {errors.email && (
          <Alert
            severity="error"
          >
            {errors.email.message}
          </Alert>
        )}
        <TextField
          label="Phone Number *"
          fullWidth
          variant="outlined"
          name="phone"
          autoComplete="phone"
          error={!!errors.phone}
          inputRef={register}
          placeholder="Phone Number"
          onBlur={handleVerifyPhone}
        />
        {errors.phone && (
          <Alert
            severity="error"
          >
            {errors.phone.message}
          </Alert>
        )}
        <PasswordField
          error={!!errors.password}
          register={register}
        />
        {errors.password && (
          <Alert
            severity="error"
          >
            {errors.password.message}
          </Alert>
        )}
        <Button
          color="primary"
          className={submitBtn}
          type="submit"
          fullWidth
          disabled={!sbmt}
        >
          next
        </Button>
      </form>
    </>
  );
};

CreateAccount.propTypes = {
  type: PropTypes.oneOf(['student', 'school', 'mentor'])
};

CreateAccount.defaultProps = {
  type: 'student'
};

export default CreateAccount;
