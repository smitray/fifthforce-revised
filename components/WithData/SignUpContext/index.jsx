import React, { useContext } from 'react';
import { useRouter } from 'next/router';
import gql from 'graphql-tag';
import cookie from 'js-cookie';

import {
  useApolloClient,
  useQuery
} from '@apollo/react-hooks';

import {
  BackDrop
} from '~base';

import { AuthContext } from '~lib/authContext';

const GET_ME = gql`
  {
    myself {
      account{
        username
        accType
        part
        status
      }
    }
  }
`;

const SignUpContext = () => {
  const router = useRouter();
  const { setUser } = useContext(AuthContext);
  const client = useApolloClient();

  const {
    data: { myself } = {},
    loading
  } = useQuery(GET_ME, {
    onCompleted: async ({ myself: my }) => {
      const token = cookie.get('token');
      if (!token) {
        await client.clearStore();
        router.push('/');
      } else if (my.account.status === 'active') {
        router.push('/auth');
      } else {
        setUser(my);
      }
    },
    onError: async () => {
      cookie.remove('token');
      await client.clearStore();
      router.push('/');
    }
  });

  if (!myself) {
    return (
      <BackDrop
        open={loading || !myself}
      />
    );
  }

  return true;
};


export default SignUpContext;
