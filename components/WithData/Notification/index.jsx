import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import Link from 'next/link';

import {
  ListItem,
  ListItemText,
  ListItemAvatar,
  Avatar
} from '@material-ui/core';

import {
  CheckCircle,
  Error,
  Info,
  NotificationImportant
} from '@material-ui/icons';

import {
  NotificationStyle
} from '~style';

const Notification = ({
  data
}) => {
  const {
    success,
    warning,
    info,
    danger,
    listStyle
  } = NotificationStyle();

  return (
    <>
      {data.map((item) => (
        <Link
          key={item._id}
          href={item.url}
          passHref
        >
          <ListItem
            className={listStyle}
            component="a"
            button
          >
            <ListItemAvatar>
              <Avatar
                className={clsx({
                  [danger]: item.nType === 'danger',
                  [success]: item.nType === 'success',
                  [warning]: item.nType === 'warning',
                  [info]: item.nType === 'info'
                })}
              >
                {item.nType === 'danger' && (
                  <Error />
                )}
                {item.nType === 'success' && (
                  <CheckCircle />
                )}
                {item.nType === 'warning' && (
                  <NotificationImportant />
                )}
                {item.nType === 'info' && (
                  <Info />
                )}
              </Avatar>
            </ListItemAvatar>
            <ListItemText
              primary={item.content}
            />
          </ListItem>
        </Link>
      ))}
    </>
  );
};

Notification.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape({
    content: PropTypes.string,
    nType: PropTypes.string,
    url: PropTypes.string
  })).isRequired
};

export default Notification;
