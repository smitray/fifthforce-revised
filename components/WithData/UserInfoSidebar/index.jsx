import React from 'react';
import PropTypes from 'prop-types';

import {
  Grid,
  Avatar
} from '@material-ui/core';

import {
  SidebarStyle,
  UserInfoTypo
} from '~style';


const UserInfoSidebar = ({
  user
}) => {
  const {
    avatar
  } = SidebarStyle();

  return (
    <>
      <Grid
        container
        justify="center"
        spacing={0}
      >
        <Avatar
          alt={user.name}
          src={user.avatar ? `/${user.avatar.filePath}` : null}
          className={avatar}
        >
          {user.name.charAt(0).toUpperCase()}
        </Avatar>
      </Grid>
      <UserInfoTypo
        align="center"
        component="h4"
        variant="button"
      >
        {user.account.accType === 'school' ? user.school.name : user.name}
      </UserInfoTypo>
      <UserInfoTypo
        align="center"
        variant="subtitle1"
        gutterBottom
        paragraph
        color="textSecondary"
      >
        {user.account?.accType}
      </UserInfoTypo>
    </>
  );
};

UserInfoSidebar.propTypes = {
  user: PropTypes.shape({
    name: PropTypes.string,
    avatar: PropTypes.shape({
      filePath: PropTypes.string
    }),
    account: PropTypes.shape({
      accType: PropTypes.string
    }),
    school: PropTypes.shape({
      name: PropTypes.string
    })
  }).isRequired
};

export default UserInfoSidebar;
