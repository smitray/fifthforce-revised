import React from 'react';
import PropTypes from 'prop-types';
import gql from 'graphql-tag';

import {
  useQuery
} from '@apollo/react-hooks';

import {
  AutoComplete,
  BackDrop
} from '~base';

const GET_SCHOOLS = gql`
  query getSchools($districtId: String!){
    schools(
      districtId: $districtId
    ){
      name
      _id
      address
      phone
      medium
      sType
    }
  }
`;

const SchoolDropdown = ({
  districtId,
  ...other
}) => {
  const {
    data: { schools } = [],
    loading
  } = useQuery(GET_SCHOOLS, {
    variables: {
      districtId
    },
    skip: !districtId
  });

  if (!districtId) {
    return true;
  }

  if (districtId && (!schools || loading)) {
    return (
      <BackDrop
        open={loading || !schools}
      />
    );
  }

  return (
    <>
      <AutoComplete
        data={schools}
        dataKey="name"
        {...other}
      />
    </>
  );
};

SchoolDropdown.propTypes = {
  districtId: PropTypes.string.isRequired
};

export default SchoolDropdown;
