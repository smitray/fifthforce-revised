import React, {
  useState
} from 'react';
import PropTypes from 'prop-types';
import Link from 'next/link';
import { useRouter } from 'next/router';

import clsx from 'clsx';
import {
  List,
  ListItem,
  ListItemText,
  ListItemIcon,
  Collapse,
  Icon
} from '@material-ui/core';
import {
  ExpandMore,
  ExpandLess
} from '@material-ui/icons';

import { SidebarStyle } from '~style';

const Navigation = ({
  item
}) => {
  const [collapsed, setCollapsed] = useState(true);
  const {
    label,
    items,
    iconName,
    url
  } = item;

  const {
    activeColor,
    listItemTypo,
    activeBackGround,
    arrowColor,
    icon,
    iconDefault
  } = SidebarStyle();

  function toggleCollapse() {
    setCollapsed((prevValue) => !prevValue);
  }

  function onClick() {
    if (Array.isArray(items)) {
      toggleCollapse();
    }
  }

  let expandIcon;

  if (Array.isArray(items) && items.length) {
    expandIcon = !collapsed ? (
      <ExpandLess className={arrowColor} />
    ) : (
      <ExpandMore className={arrowColor} />
    );
  }

  const router = useRouter();

  return (
    <>
      {url ? (
        <Link href={url} passHref>
          <ListItem
            component="a"
            button
            selected={router.pathname === url}
            className={clsx({
              [activeColor]: router.pathname === url
            })}
          >
            <ListItemIcon>
              <Icon
                className={clsx({
                  [icon]: iconName,
                  [iconDefault]: !iconName
                })}
              >
                {iconName || 'radio_button_unchecked'}
              </Icon>
            </ListItemIcon>
            <ListItemText primary={label} className={listItemTypo} />
          </ListItem>
        </Link>
      ) : (
        <ListItem
          button
          onClick={onClick}
          className={clsx({
            [activeBackGround]: !collapsed
          })}
        >
          <ListItemIcon>
            <Icon
              className={clsx({
                [icon]: iconName,
                [iconDefault]: !iconName
              })}
            >
              {iconName || 'radio_button_unchecked'}
            </Icon>
          </ListItemIcon>
          <ListItemText primary={label} className={listItemTypo} />
          {expandIcon}
        </ListItem>
      )}
      <Collapse in={!collapsed} timeout="auto" unmountOnExit>
        {Array.isArray(items) ? (
          <List component="nav" disablePadding>
            {items.map((subItem) => (
              <Navigation
                item={subItem}
                key={`${subItem.name}-${subItem.id}`}
              />
            ))}
          </List>
        ) : null}
      </Collapse>
    </>
  );
};

Navigation.propTypes = {
  item: PropTypes.shape({
    id: PropTypes.number.isRequired,
    label: PropTypes.string.isRequired,
    url: PropTypes.string,
    iconName: PropTypes.string,
    items: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number.isRequired,
      label: PropTypes.string.isRequired,
      url: PropTypes.string
    }))
  }).isRequired
};

export default Navigation;
