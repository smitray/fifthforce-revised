import React from 'react';
import gql from 'graphql-tag';

import {
  useQuery
} from '@apollo/react-hooks';

import { AutoComplete } from '~base';

const GET_COURSES = gql`
  query{
    courses{
      _id
      name
    }
  }
`;

const CourseDropdown = ({
  ...other
}) => {
  const {
    data: { courses } = [],
    loading
  } = useQuery(GET_COURSES);

  if (!courses || loading) {
    return true;
  }

  return (
    <>
      <AutoComplete
        data={courses}
        dataKey="name"
        {...other}
      />
    </>
  );
};

export default CourseDropdown;
