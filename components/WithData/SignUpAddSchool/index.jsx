import React, {
  useState,
  useContext,
  useEffect
} from 'react';

import { useRouter } from 'next/router';
import gql from 'graphql-tag';
import * as yup from 'yup';

import {
  useMutation
} from '@apollo/react-hooks';

import {
  Alert
} from '@material-ui/lab';

import {
  useForm
} from 'react-hook-form';

import {
  TextField,
  AuthStyle,
  HeaderTypo
} from '~style';

import {
  Button,
  AutoComplete,
  DatePicker
} from '~base';

import { AuthContext } from '~lib/authContext';

import Courses from '../CourseDropdown';
import Districts from '../DistrictDropdown';
import School from '../SchoolDropdown';
import SignUpNewSchool from '../SignUpNewSchool';


const studentSchema = yup.object().shape({
  course: yup
    .string()
    .required('You need to select your course'),
  courseName: yup
    .string(),
  school: yup
    .string()
    .when('courseName', {
      is: (courseName) => courseName !== 'CRT',
      then: yup
        .string()
        .required('School is required')
    }),
  degree: yup
    .string()
    .when('courseName', {
      is: (courseName) => courseName === 'CRT',
      then: yup
        .string()
        .required('Your degree is required')
    }),
  yop: yup
    .date()
    .when('courseName', {
      is: (courseName) => courseName === 'CRT',
      then: yup
        .date()
        .required('Year of Passing is required')
        .max(new Date(), 'Year of Passing can not be in the future')
        .typeError('Year of Passing has to be a valid date')
    }),
  college: yup
    .string()
    .when('courseName', {
      is: (courseName) => courseName === 'CRT',
      then: yup
        .string()
        .required('University / College name is required')
    }),
  address: yup
    .string()
    .when('courseName', {
      is: (courseName) => courseName === 'CRT',
      then: yup
        .string()
        .required('University / College address is required')
    }),
  ucState: yup
    .string()
    .when('courseName', {
      is: (courseName) => courseName === 'CRT',
      then: yup
        .string()
        .required('University / College state is required')
    })
});

const schoolSchema = yup.object().shape({
  course: yup
    .string()
    .required('You need to select your course')
});

const crtDegreeData = [{
  value: 'B.Tech.'
}, {
  value: 'B.E.'
}, {
  value: 'B.ARCH.'
}, {
  value: 'BCA'
}, {
  value: 'BBA'
}, {
  value: 'B.Sc.'
}, {
  value: 'B.Com.'
}, {
  value: 'B.A.'
}, {
  value: 'MBA'
}, {
  value: 'MCA'
}, {
  value: 'M.Tech.'
}, {
  value: 'M.Sc.'
}, {
  value: 'M.Com.'
}, {
  value: 'M.A.'
}];

const statesData = [{ code: 'AN', name: 'Andaman and Nicobar Islands' },
  { code: 'AP', name: 'Andhra Pradesh' },
  { code: 'AR', name: 'Arunachal Pradesh' },
  { code: 'AS', name: 'Assam' },
  { code: 'BR', name: 'Bihar' },
  { code: 'CG', name: 'Chandigarh' },
  { code: 'CH', name: 'Chhattisgarh' },
  { code: 'DH', name: 'Dadra and Nagar Haveli' },
  { code: 'DD', name: 'Daman and Diu' },
  { code: 'DL', name: 'Delhi' },
  { code: 'GA', name: 'Goa' },
  { code: 'GJ', name: 'Gujarat' },
  { code: 'HR', name: 'Haryana' },
  { code: 'HP', name: 'Himachal Pradesh' },
  { code: 'JK', name: 'Jammu and Kashmir' },
  { code: 'JH', name: 'Jharkhand' },
  { code: 'KA', name: 'Karnataka' },
  { code: 'KL', name: 'Kerala' },
  { code: 'LD', name: 'Lakshadweep' },
  { code: 'MP', name: 'Madhya Pradesh' },
  { code: 'MH', name: 'Maharashtra' },
  { code: 'MN', name: 'Manipur' },
  { code: 'ML', name: 'Meghalaya' },
  { code: 'MZ', name: 'Mizoram' },
  { code: 'NL', name: 'Nagaland' },
  { code: 'OR', name: 'Odisha' },
  { code: 'PY', name: 'Puducherry' },
  { code: 'PB', name: 'Punjab' },
  { code: 'RJ', name: 'Rajasthan' },
  { code: 'SK', name: 'Sikkim' },
  { code: 'TN', name: 'Tamil Nadu' },
  { code: 'TS', name: 'Telangana' },
  { code: 'TR', name: 'Tripura' },
  { code: 'UK', name: 'Uttarakhand' },
  { code: 'UP', name: 'Uttar Pradesh' },
  { code: 'WB', name: 'West Bengal' }];


const AddSchool = () => {
  const {
    submitBtn,
    dobStyle,
    addSchoolBtn
  } = AuthStyle();

  const [crtWindow, setCrtWindow] = useState(false);
  const [districtId, setDistrictId] = useState('');
  const [addSchoolModal, setAddSchoolModal] = useState(false);

  const { user } = useContext(AuthContext);

  const {
    register,
    handleSubmit,
    errors,
    setValue
  } = useForm({
    validationSchema: user?.account.accType === 'student' ? studentSchema : schoolSchema
  });

  useEffect(() => {
    register({ name: 'course' });
    register({ name: 'courseName' });
    register({ name: 'school' });
    register({ name: 'degree' });
    register({ name: 'yop' });
    register({ name: 'ucState' });
  }, [register]);

  const handleCourse = (e, { name, _id }) => {
    if (name === 'CRT') {
      setCrtWindow(true);
    } else {
      setCrtWindow(false);
    }
    setValue('course', _id);
    setValue('courseName', name);
  };

  const schoolModalToggle = () => {
    setAddSchoolModal(!addSchoolModal);
  };

  const handleLocation = (e, { _id }) => {
    setDistrictId(_id);
  };

  const handleSchool = (e, { _id }) => {
    setValue('school', _id);
  };

  const handleDegree = (e, { value }) => {
    setValue('degree', value);
  };

  const handleYear = (date) => {
    setValue('yop', date);
  };

  const handleState = (e, { name }) => {
    setValue('ucState', name);
  };

  const handleSignUp = (input) => {
    delete input.courseName;
    console.log(input);
  };

  if (!user) {
    return null;
  }

  return (
    <>
      <form onSubmit={handleSubmit(handleSignUp)}>
        {user.account.accType === 'school' ? (
          <>
            <HeaderTypo
              variant="h3"
              component="h3"
              align="center"
              gutterBottom
            >
              add school
            </HeaderTypo>
            <HeaderTypo
              variant="subtitle1"
              align="center"
              color="textSecondary"
              gutterBottom
              paragraph
            >
              Please select the school from the list
            </HeaderTypo>
          </>
        ) : (
          <>
            <HeaderTypo
              variant="h3"
              component="h3"
              align="center"
              gutterBottom
            >
              select your course
            </HeaderTypo>
            <HeaderTypo
              variant="subtitle1"
              align="center"
              color="textSecondary"
              gutterBottom
              paragraph
            >
              Please select the course
            </HeaderTypo>
            <Courses
              dataCallBack={handleCourse}
              label="Please select your course *"
              error={!!errors.course}
            />
            {errors.course && (
            <Alert
              severity="error"
            >
              {errors.course.message}
            </Alert>
            )}
          </>
        )}
        {crtWindow ? (
          <>
            <AutoComplete
              data={crtDegreeData}
              dataKey="value"
              dataCallBack={handleDegree}
              label="Select your degree *"
              error={!!errors.degree}
            />
            {errors.degree && (
              <Alert
                severity="error"
              >
                {errors.degree.message}
              </Alert>
            )}
            <DatePicker
              views={['year']}
              cbDate={handleYear}
              label="Year of Passing"
              disableFuture
              fullWidth
              className={dobStyle}
              error={!!errors.yop}
            />
            {errors.yop && (
              <Alert
                severity="error"
              >
                {errors.yop.message}
              </Alert>
            )}
            <TextField
              label="University / College name *"
              fullWidth
              variant="outlined"
              name="college"
              error={!!errors.college}
              inputRef={register}
              placeholder="University / College name"
            />
            {errors.college && (
              <Alert
                severity="error"
              >
                {errors.college.message}
              </Alert>
            )}
            <TextField
              label="University / College address *"
              fullWidth
              variant="outlined"
              name="address"
              error={!!errors.address}
              inputRef={register}
              placeholder="University / College address"
            />
            {errors.address && (
              <Alert
                severity="error"
              >
                {errors.address.message}
              </Alert>
            )}
            <AutoComplete
              data={statesData}
              dataKey="name"
              dataCallBack={handleState}
              label="State of your University / College *"
              error={!!errors.ucState}
            />
            {errors.ucState && (
              <Alert
                severity="error"
              >
                {errors.ucState.message}
              </Alert>
            )}
          </>
        ) : (
          <>
            <Districts
              dataCallBack={handleLocation}
              label="District / City of your school *"
            />
            <School
              dataCallBack={handleSchool}
              label="School"
              districtId={districtId}
            />
            {errors.school && (
            <Alert
              severity="error"
            >
              {errors.school.message}
            </Alert>
            )}
            <Button
              variant="text"
              className={addSchoolBtn}
              onClick={schoolModalToggle}
            >
              Click here if you do not find your school
            </Button>
          </>
        )}
        <Button
          color="primary"
          className={submitBtn}
          type="submit"
          fullWidth
        >
          next
        </Button>
      </form>
      <SignUpNewSchool
        open={addSchoolModal}
        title="Add New School"
        handleModalClose={schoolModalToggle}
      />
    </>
  );
};


export default AddSchool;
