import React, {
  useEffect,
  useContext
} from 'react';

import { useRouter } from 'next/router';
import gql from 'graphql-tag';
import * as yup from 'yup';

import {
  useMutation
} from '@apollo/react-hooks';

import {
  Alert
} from '@material-ui/lab';

import {
  useForm
} from 'react-hook-form';

import {
  TextField,
  AuthStyle,
  HeaderTypo
} from '~style';

import {
  AutoComplete,
  Button,
  DatePicker
} from '~base';

import { AuthContext } from '~lib/authContext';

const schema = yup.object().shape({
  name: yup
    .string()
    .required(),
  gender: yup
    .string()
    .oneOf(['male', 'female', 'other'])
    .required('Gender is required'),
  dob: yup
    .date()
    .required('Birth date is required')
    .max(new Date(), 'Birth date can not be in the future')
    .typeError('Birth date has to be a valid date')
});

const genderData = [{
  name: 'Male',
  value: 'male'
}, {
  name: 'Female',
  value: 'female'
}, {
  name: 'Others',
  value: 'others'
}];

const CREATE_USER = gql`
  mutation userCreate(
    $name: String!,
    $dob: DateTime!,
    $gender: String!
  ){
    userCreate(
      name: $name,
      dob: $dob,
      gender: $gender
    ){
      name
    }
  }
`;

const CreatePersonalDetails = () => {
  const {
    submitBtn,
    dobStyle
  } = AuthStyle();

  const {
    register,
    handleSubmit,
    errors,
    setValue
  } = useForm({
    validationSchema: schema
  });

  const router = useRouter();

  const { user } = useContext(AuthContext);

  useEffect(() => {
    register({ name: 'gender' });
    register({ name: 'dob' });
  }, [register]);

  const handleGender = (e, { value }) => {
    setValue('gender', value);
  };

  const handleDate = (date) => {
    setValue('dob', new Date(date));
  };

  const [signUser, {
    error
  }] = useMutation(
    CREATE_USER
  );

  const handleSignUp = async (input) => {
    await signUser({
      variables: {
        ...input
      }
    });
    router.push(`/signup/${user.account.accType}/2`);
  };

  return (
    <>
      <HeaderTypo
        variant="h3"
        component="h3"
        align="center"
        gutterBottom
      >
        personal details
      </HeaderTypo>
      <HeaderTypo
        variant="subtitle1"
        align="center"
        color="textSecondary"
        gutterBottom
        paragraph
      >
        Please tell us more about you
      </HeaderTypo>
      <form onSubmit={handleSubmit(handleSignUp)}>
        {error && (
          <Alert
            severity="error"
          >
            {error.message.replace('GraphQL error:', '').trim()}
          </Alert>
        )}
        <TextField
          label="Full Name *"
          fullWidth
          variant="outlined"
          autoComplete="name"
          name="name"
          error={!!errors.name}
          inputRef={register}
          placeholder="Full Name"
        />
        {errors.name && (
          <Alert
            severity="error"
          >
            {errors.name.message}
          </Alert>
        )}
        <AutoComplete
          data={genderData}
          dataKey="name"
          dataCallBack={handleGender}
          label="Select your gender *"
          error={!!errors.gender}
        />
        {errors.gender && (
          <Alert
            severity="error"
          >
            {errors.gender.message}
          </Alert>
        )}
        <DatePicker
          openTo="year"
          cbDate={handleDate}
          format="do MMMM, yyyy"
          label="Date of Birth"
          disableFuture
          fullWidth
          className={dobStyle}
          error={!!errors.dob}
        />
        {errors.dob && (
          <Alert
            severity="error"
          >
            {errors.dob.message}
          </Alert>
        )}
        <Button
          color="primary"
          className={submitBtn}
          type="submit"
          fullWidth
        >
          next
        </Button>
      </form>
    </>
  );
};


export default CreatePersonalDetails;
