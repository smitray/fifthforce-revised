import React, {
  useState,
  useEffect
} from 'react';
import PropTypes from 'prop-types';

import gql from 'graphql-tag';
import * as yup from 'yup';

import {
  useMutation
} from '@apollo/react-hooks';

import {
  useForm
} from 'react-hook-form';

import {
  Alert
} from '@material-ui/lab';

import {
  Button,
  ModalBox
} from '~base';

import {
  TextField
} from '~style';

import Districts from '../DistrictDropdown';

const addSchoolSchema = 1;

const SignUpNewSchool = ({
  handleModalClose,
  ...other
}) => {
  const {
    register,
    handleSubmit,
    errors,
    setValue
  } = useForm({
    validationSchema: addSchoolSchema
  });

  useEffect(() => {
    register({ name: 'districtId' });
  }, [register]);


  const handleLocation = (e, { _id }) => {
    setValue('districtId', _id);
  };

  const handleAddSchool = (input) => {
    console.log(input);
  };

  return (
    <>
      <form onSubmit={handleSubmit(handleAddSchool)}>
        <ModalBox
          {...other}
          actions={(
            <>
              <Button
                onClick={handleModalClose}
                color="primary"
                autoFocus
                variant="text"
              >
                Close
              </Button>
              <Button
                color="secondary"
                variant="text"
                type="submit"
              >
                Add new school
              </Button>
            </>
          )}
        >
          <Districts
            dataCallBack={handleLocation}
            label="District / City of your school *"
          />
          <TextField
            label="School Name"
            margin="normal"
            fullWidth
            variant="outlined"
            name="schoolName"
            placeholder="School Name"
          />
          <TextField
            label="School Address"
            margin="normal"
            fullWidth
            variant="outlined"
            name="address"
            placeholder="School Address"
            multiline
          />
        </ModalBox>
      </form>
    </>
  );
};

SignUpNewSchool.propTypes = {
  handleModalClose: PropTypes.func.isRequired
};


export default SignUpNewSchool;
