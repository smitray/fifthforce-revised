import React from 'react';
import gql from 'graphql-tag';

import {
  useQuery
} from '@apollo/react-hooks';

import { AutoComplete } from '~base';

const GET_LOCATIONS = gql`
  query{
    districts{
      _id
      name
    }
  }
`;


const DistrictDropdown = ({
  ...other
}) => {
  const {
    data: { districts } = [],
    loading
  } = useQuery(GET_LOCATIONS);

  if (!districts || loading) {
    return true;
  }

  return (
    <>
      <AutoComplete
        data={districts}
        dataKey="name"
        {...other}
      />
    </>
  );
};


export default DistrictDropdown;
